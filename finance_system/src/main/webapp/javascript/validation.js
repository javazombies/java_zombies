function validateForm() {
	var formName = "bondCalculator";
	var loanAmount = document.forms[formName]["loan"].value;
	var interestRate = document.forms[formName]["rate"].value;
	var deposit = document.forms[formName]["deposit"].value;
	var term = document.forms[formName]["term"].value;
	var fields = [ loanAmount, interestRate, deposit, term ];

	if(!checkEmptyAndNonNumerics(fields))
		return false;
	if (!allValuesValid(fields)) 
		return false;
	
	return true;

}

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function checkEmptyAndNonNumerics(fields){
	for (var i = 0; i < fields.length; i++) {
		if (fields[i] == "") {
			clearDiv();
			document.getElementById('error-div').innerHTML = "<i class='fa fa-times-circle'></i> A required field has been left blank.";
			show();
			return false;
		}
		if (!isNumber(fields[i])) {
			clearDiv();
			document.getElementById('error-div').innerHTML = "<i class='fa fa-times-circle'></i> All input should be numeric.";
			show();
			return false;
		}
	}
	return true;
}

function allValuesValid(fields) {
	if (parseFloat(fields[0]) <= parseFloat(fields[2])) {
		clearDiv();
		document.getElementById('error-div').innerHTML = "<i class='fa fa-times-circle'></i> Deposit amount cannot be GREATER or EQUAL to the purchase amount.";
		show();
		return false;
	}
	if (parseFloat(fields[3]) <= 0 || parseFloat(fields[1]) <= 0) {
		clearDiv();
		document.getElementById('error-div').innerHTML = "<i class='fa fa-times-circle'></i> Both the term and interest rate cannot be zero.";
		show();
		return false;
	}
	for (var i = 0; i < fields.length; i++) {
		if (fields[i] < 0) {
			clearDiv();
			document.getElementById('error-div').innerHTML = "<i class='fa fa-times-circle'></i> All numbers should be positive.";
			show();
			return false;
		}
	}
	
	return true;
}

function validateInvestmentForm() {
	var formName = "investmentCalculator";
	var monthlyContribution = document.forms[formName]["monthlyContribution"].value;
	var investmentTerm = document.forms[formName]["investmentTerm"].value;
	var rate = document.forms[formName]["rate"].value;
	var fields = [ monthlyContribution, investmentTerm, rate ];
	
	if(!checkEmptyAndNonNumerics(fields))
		return false;
	
	for (var i = 0; i < fields.length; i++) {
		if (fields[i] < 0) {
			clearDiv();
			document.getElementById('error-div').innerHTML = "<i class='fa fa-times-circle'></i> All numbers should be positive.";
			show();
			return false;
		}
	}
	return true;
}

function show() {
	document.getElementById('error-div').hidden = false;
}

function clearDiv() {
	document.getElementById('error-div').innerHTML = "";
}
