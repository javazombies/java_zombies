<%@page import="com.javazombiez.finance.model.Investment"%>
<%@page import="com.javazombiez.finance.utility.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../bootstrap.css" />
<link href="../css/index-page.css" rel="stylesheet">
<title>create investor</title>
<style type="text/css">
form {
	font-family: arial, sans-serif;
	width
	="60%";
}
</style>
</head>

<body>
	<div class="container">

		<h2 align="center">Implement Investment</h2>
		<hr>

		<table>
			<tr>
				<td style="width: 636px;">
					<div class="panel panel-success" style="width: 650px;">
						<div class="panel-heading HeadingOverride">
							<b>Existing Investment Client</b>
						</div>
						<div class="panel-body">
							<div style="text-align: center;">
								<form align="center" class="form-inline"
									action="<%=request.getContextPath()%>/investment/addinvestmenttoexistingclient"
									method="post">
									<table>
										<tr>
											<td align="left"><label>ID Number: </label><input
												class="form-control" type='number' name='identity_number'
												required="true" style="width: 269px;"
												placeholder="Enter ID Number " /> <input
												class="btn btn-default" type="submit" value="Add Investment"
												style="width: 213px; height: 35px" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</td>
				<td>&nbsp;&nbsp;&nbsp;</td>
				<td style="column-width: 50%; width: 577px">
					<div class="panel panel-success">
						<div class="panel-heading HeadingOverride">
							<b>Selected Investment</b>
						</div>
						<div class="panel-body">
							<div style="text-align: center;">
								<table align="center">
									<tr align="left">
										<%
											Investment investment = (Investment) request.getAttribute("investment");
											request.getSession().setAttribute("investmentImpl", investment);
										%>
										<td align="left"><label>Monthly Contribution : </label></td>
										<td align="right"><label><%=investment.getContribution()%></label></td>
										<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Interest
												Rate : </label></td>
										<td align="right"><label><%=investment.getInterestRate()%>
										</label></td>
									</tr>
									<tr align="left">
										<td align="left"><label>Term : </label></td>
										<td align="right"><label><%=investment.getTerm()%></label></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
		<hr />
		<div class="panel panel-success">
			<div class="panel-heading HeadingOverride">
				<b>New Investment Client</b>
			</div>
			<div class="panel-body">
				<div style="text-align: center;">
					<form align="center" class="form-inline"
						action="<%=request.getContextPath()%>/investment/add "
						method="post">
						<div class="form-group">
							<table style="width: 1172px;">
								<tr>
									<td style="width: 686px; height: 60px;" align="left"
										colspan="2"><label>First Name:</label> <input
										class="form-control" type="text" name="first_name"
										required="true" style="width: 602px;" /></td>
									<td style="width: 449px; height: 60px" align="left"><label>LastName:</label>
										<input class="form-control" type='text' name='surname'
										required style="width: 302px;" /></td>
								</tr>
								<tr>
									<td style="width: 317px; height: 60px" align="left"><label>Date
											Of Birth:</label> <input class="form-control" type='date'
										name='date_of_birth' required="true" style="width: 192px;" /></td>
									<td style="width: 377px; height: 60px" align="left"><label>ID
											Number: </label> <input class="form-control" type='number'
										name='identity_number' required="true" style="width: 278px;" /></td>
									<td align="left"><label>Gender:</label> <select
										class="form-control" name="gender" style="width: 319px;">
											<option value="Male" selected="selected">Male</option>
											<option value="Female">Female</option>
									</select></td>
								</tr>
								<tr>
									<td style="height: 60px" align="left"><label>Phone
											Number: </label> <input class="form-control" type='number'
										name='contact_number' required="true" style="width: 175px;" /></td>
									<td style="height: 60px" align="left"><label>Email:
									</label> <input class="form-control" type="email" name='email'
										required="true" style="width: 311px;" /></td>
									<td style="height: 60px" align="left"><label>Tax
											Number:</label> <input class="form-control" type='number'
										label="Tax Number" name='tax_number' required="true"
										style="width: 288px;" /></td>
								</tr>
								<tr>
									<td align="left"><input class="btn btn-default"
										type="submit" value="Create Client" style="width: 213px;" /></td>
								</tr>
							</table>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
		<jsp:include page="../includes/footer.html" />
		<script src="../js/bootstrap.min.js"></script>
	<script src="../vendor/jquery/jquery.min.js"></script>
	<script src="../vendor/tether/tether.min.js"></script>
</body>
</html>