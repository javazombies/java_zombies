<%@page import="com.javazombiez.finance.model.Investment"%>
<%@page import="com.javazombiez.finance.model.*"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.Map"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../bootstrap.css" />
<style type="text/css">
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 80%;
	margin: auto;
}

td, th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #dddddd;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show forecast</title>
</head>

<body>
	<div class="conatiner" style="text-align: center;">
	<%
	Investment investment = (Investment) request.getSession().getAttribute("investment");
	%>
	
		<h1 align="center">Investment Forecast</h1>
		<br />

		<h2>
			Your investment over
			<%=investment.getTerm()%>
			months.
		</h2>	
						
	<%
		
		  Forecast forecast = (Forecast) request.getAttribute("forecast");
					List<ForecastItem> items = forecast.getForcastItems();

					out.println("<table><tr>"
							+ "<th>Month </th><th>Opening Balance </th><th> Monthly Contribution</th><th>Interest Rate </th> <th>Interest Earned </th><th>Closing Balance </th></tr>");
					for (int i = 0; i < items.size(); i++) {
						out.println("<tr><td>" + items.get(i).getMonth() + "</td>" + "<td>" + items.get(i).getOpeningBalance()
								+ "</td>" + "<td>" + items.get(i).getContribution() + "</td>" + "<td>"
								+ items.get(i).getInterestRate() + " </td>" + "<td>" + items.get(i).getInterestEarned()
								+ "</td>" + "</td>" + "<td>" + items.get(i).getClosingBalance() + "</td>" + "</tr>");

					}
					out.println("</table><br>");
		%>


		<form action="<%=request.getContextPath()%>/investment/investor"
			method="get">
			<%
			  request.getSession().setAttribute("investment_imp", investment);
			%>

			<input type="submit" class="btn btn-default" onclick="goBack()"
				style="width: 15%;" value="Back to calculator" /> <input
				type="submit" class="btn btn-default" style="width: 15%;"
				value="Implement this Investment" />
		</form>
	</div>
	<script type="text/javascript">
		function goBack() {
			window.history.back();
		}
	</script>
</body>
</html>