<%@page import="com.javazombiez.finance.model.*"%>
<%@page import="java.util.*"%>
<%@page import="com.javazombiez.finance.controller.*"%>
<%@page import="com.javazombiez.finance.utility.Money"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html id="htmlInvest">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Custom css -->
<link href="../css/styles1.css" rel="stylesheet">
<link href="../css/styles2.css" rel="stylesheet">
<link href="../css/styles3.css" rel="stylesheet">
<link rel="icon" href="../img/penny.ico">
<!-- Bootstrap -->
<!-- <link href="../css/bootstrap.min.css" rel="stylesheet"> -->
<link href="../css/index-page.css" rel="stylesheet">
<link href="../vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic"
	rel="stylesheet" type="text/css">
<link href="../css/java_zombies.css" rel="stylesheet">


<title>investment Calculator</title>
</head>
<body id="bodyInvest">

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1 style="text-align: center">Client Details</h1>
				<div>&nbsp;</div>
			</div>
			<hr>
		</div>

		<div id="left-panel" style="float: left; width: 30%;">

			<div class="panel panel-success" style="width: 505px;">
				<div class="panel-heading HeadingOverride">Clients</div>
				<div class="panel-body">
					<table width="100%">
						<tr>
							<th><u>First Name</u></th>
							<th><u>Last Name</u></th>
							<th><u>Client Number</u></th>
							<th>&emsp;</th>
						</tr>
						<%
							List<Investor> investors = (List<Investor>) request.getSession().getAttribute("investors");
							if (investors != null && investors.size() > 0) {
								for (Investor investor : investors) {
									out.println("<form action='" + request.getContextPath() + "/investment/clientproducts' method='post'>"
											+ "<tr>" + "<td>" + investor.getName() + "&emsp;" + "</td>" + "<td>" + investor.getSurname() + "&emsp;"
											+ "</td>" + "<td>" + investor.getInvestorNumber() + "&emsp;&emsp;" + "</td>"
											+ "<td><button type='submit' name='investornumber' value='" + investor.getInvestorNumber()
											+ "' class='btn btn-default'>Show Products </button></td>" + "</tr>" + "</form>");
								}
							}
						%>
					</table>
				</div>
			</div>
		</div>

		<div id="right-panel" style="float: right; width: 65%;">
			<div class="row">
				<div class="col-xs-11">
					<div class="panel panel-success">
						<div class="panel-heading">Client Products</div>
						<div class="panel-body">
							<%
								List<PropertyBond> bondProducts = (List<PropertyBond>) request.getSession().getAttribute("bondProducts");
								List<Investment> investmentProducts = (List<Investment>) request.getSession().getAttribute("investmentProducts");

								if (bondProducts != null && bondProducts.size() > 0) {
									out.println("<table width='100%'>" + "<tr><th colspan='5'>Bonds</th></tr>" + "<tr>"
											+ "<th><u>Principal Amount</u>&emsp;&emsp;</th>" + "<th><u>Monthly Payment</u>&emsp;&emsp;</th>"
											+ "<th><u>Interest rate</u>&emsp;&emsp;</th>" + "<th><u>Term</u></th>" + "<th>&emsp;</th>" + "</tr>");

									for (PropertyBond bond : bondProducts) {
										out.println("<form action='" + request.getContextPath() + "/investment/bondevents' method='post'>" + "<tr>"
												+ "<td>" + bond.getPrincipalAmount() + "&emsp;" + "</td>" + "<td>" + bond.getMonthlyPayment() + "&emsp;"
												+ "</td>" + "<td>" + bond.getInterestRate() + "&emsp;&emsp;" + "</td>" + "<td>" + bond.getTerm()
												+ "&emsp;&emsp;" + "</td>" + "<td><button type='submit' name='bondid' value='" + bond.getId()
												+ "' class='btn btn-default'>Show Events</button></td>" + "</tr>" + "</form>");
									}
									out.println("</table><br><hr>");
								}
								if (investmentProducts != null && investmentProducts.size() > 0) {
									out.println("<table width='100%'>" + "<tr><th colspan='4'>Investments</th></tr>" + "<tr>"
											+ "<th><u>Monthly Contribution</u>&emsp;&emsp;</th>" + "<th><u>Interest rate</u>&emsp;&emsp;</th>"
											+ "<th><u>Term</u></th>" + "<th>&emsp;</th>" + "</tr>");

									for (Investment investment : investmentProducts) {
										out.println("<form action='" + request.getContextPath() + "/investment/investmentevents' method='post'>"
												+ "<tr>" + "<td>" + investment.getContribution() + "&emsp;" + "</td>" + "<td>"
												+ investment.getInterestRate() + "&emsp;" + "</td>" + "<td>" + investment.getTerm() + "&emsp;&emsp;"
												+ "</td>" + "<td><button type='submit' name='investmentid' value='" + investment.getId()
												+ "' class='btn btn-default'>Show Events</button></td>" + "</tr>" + "</form>");
									}
									out.println("</table>");
								}
							%>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-11">
					<div class="table-responsive">
						<table id="investTable"
							class="table table-striped table-bordered table-hover">
							<tbody>
								<tr>
									<th>Event #</th>
									<th>Month</th>
									<th>Interest Rate</th>
									<th>Monthly Amount</th>
								</tr>
								<%
									List<BondEvent> bondEvents = (List<BondEvent>) request.getAttribute("bondEvents");
									List<Event> investmentEvents = (List<Event>) request.getAttribute("investmentEvents");
									if (bondEvents != null) {
										for (int i = 0; i < bondEvents.size(); i++) {
											out.println("<tr><td>" + (i + 1) + "</td>" + "<td>" + bondEvents.get(i).getMonth() + "</td>" + "<td>"
													+ bondEvents.get(i).getInterestRate() + "</td>" + "<td>" + bondEvents.get(i).getMonthlyPayment()
													+ " </td>" + "</tr>");
										}
									}
									if (investmentEvents != null) {
										for (int i = 0; i < investmentEvents.size(); i++) {
											out.println("<tr><td>" + (i + 1) + "</td>" + "<td>" + investmentEvents.get(i).getMonth() + "</td>" + "<td>"
													+ investmentEvents.get(i).getInterestRate() + "</td>" + "<td>" + investmentEvents.get(i).getContribution()
													+ " </td>" + "</tr>");
										}
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../includes/footer.html" />
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/validation.js"></script>
	<script src="../vendor/jquery/jquery.min.js"></script>
	<script src="../vendor/tether/tether.min.js"></script>
</body>
</html>