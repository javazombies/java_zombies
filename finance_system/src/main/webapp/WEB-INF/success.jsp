<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../bootstrap.css" />
<link href="../css/index-page.css" rel="stylesheet">
<title>Investment</title>
</head>

<body id="bodyInvest">

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1 style="text-align: center">Create Client</h1>
				<div>&nbsp;</div>
			</div>
			<hr>
		</div>


		<div style="float: center; width: 100%;">

			<div class="panel panel-success">
				<div class="panel-heading HeadingOverride">Status</div>
				<div class="panel-body">
					<h4>Client has been Succesfully created!</h4>
				</div>
			</div>
		</div>
	</div>
	
	<jsp:include page="../includes/footer.html" />
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/validation.js"></script>
	<script src="../vendor/jquery/jquery.min.js"></script>
	<script src="../vendor/tether/tether.min.js"></script>
	
</body>
</html>
