<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="com.javazombiez.finance.model.BusinessProduct"%>
<%@page import="com.javazombiez.finance.model.PropertyBond"%>
<%@page import="com.javazombiez.finance.model.Investment"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- Custom css -->
<link href="../css/styles1.css" rel="stylesheet">
<link href="../css/styles2.css" rel="stylesheet">
<link href="../css/styles3.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
	<div class="container">
		<br> <br>
		<div class="row">
			<div class="col-xs-12 ">
				<h1 style="text-align: center">Clients Products</h1>
				<div>&nbsp;</div>
			</div>
		</div>
		<form name="clients" action="" method="post">
			<table class="table table-striped table-bordered table-hover">
				<tbody>
					<%
						List<PropertyBond> bondProducts = (List<PropertyBond>) request.getSession().getAttribute("bondProducts");
						List<Investment> investmentProducts = (List<Investment>) request.getSession().getAttribute("investmentProducts");

						if (bondProducts != null && bondProducts.size() > 0) {
							out.println("<tr><th colspan=\"5\">Property bond</th></tr>" + "<tr>" + "<th>Principal Amount</th>"
									+ "<th>Interest Rate</th>" + "<th>Monthly Payment</th>" + "<th>Term</th>" + "<th>Total Payable</th>" + "</tr>");
							for (PropertyBond bond : bondProducts) {
								out.println("<tr>" + "<td>" + bond.getPrincipalAmount() + "</td>" + "<td>" + bond.getInterestRate() + "</td>"
										+ "<td>" + bond.getMonthlyPayment() + "</td>" + "<td>" + bond.getTerm() + "</td>" + "<td>"
										+ bond.getTotalPayable() + "</td>" + "</tr>");
							}
						}

						if (investmentProducts != null && investmentProducts.size() > 0) {
							out.println("<tr><th colspan=\"4\">Investment</th></tr>" + "<tr>" + "<th>Monthly Contibution</th>"
									+ "<th>Interest Rate</th>" + "<th>Term</th>" + "<th>Future Value</th>" + "</tr>");
							for (Investment investment : investmentProducts) {
								out.println("<tr>" + "<td>" + investment.getContribution() + "</td>" + "<td>" + investment.getInterestRate()
										+ "</td>" + "<td>" + investment.getTerm() + "</td>" + "<td>" + "no get method" + "</td>" + "</tr>");
							}
						}
					%>
				</tbody>
			</table>
		</form>
	</div>
</body>
</html>