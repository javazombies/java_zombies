<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="com.javazombiez.finance.model.Investor"%>
<%@page import="com.javazombiez.finance.model.PropertyBond"%>
<%@page import="com.javazombiez.finance.model.Investment"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<!-- Custom css -->
<link href="../css/styles1.css" rel="stylesheet">
<link href="../css/styles2.css" rel="stylesheet">
<link href="../css/styles3.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="../css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
	<div  class="container">
		<br> 
		<div class="row">
			<div class="col-xs-12 ">
				<h1 style="text-align: center">Clients Details</h1>
				<div>&nbsp;</div>
			</div>
		</div>
		<form name="clients" action="" method="post">
		<table class="table table-striped table-bordered table-hover">
			<tbody>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Id No#</th>
					<th>Contact No#</th>
					<th>Client No#</th>
					<th>Products</th>
				</tr>
				<%
					List<Investor> investors = (List<Investor>) request.getAttribute("investors");
					if (investors != null) {
						for (int i=0;i<investors.size();i++) {
							Investor investor = investors.get(i);
							request.getSession().setAttribute("investor"+i, investor);
							out.println("<tr>"
							+ "<td> <a href=\""+request.getContextPath()+"/investment/investmentCalculator\"/>" + investor.getName() + "</td>" 
							+ "<td> <a href=\""+request.getContextPath()+"/investment/investmentCalculator\"/>" + investor.getSurname() + "</td>" 
							+ "<td> <a href=\""+request.getContextPath()+"/investment/investmentCalculator\"/>" + investor.getIdentityNumber() + "</td>" 
							+ "<td> <a href=\""+request.getContextPath()+"/investment/investmentCalculator\"/>" + investor.getContactNumber() + "</td>" 
							+ "<td> <a href=\""+request.getContextPath()+"/investment/investmentCalculator\"/>"+ investor.getInvestorNumber() + "</td>"
							+ "<td><input type=\"button\" class=\"btn btn-default\" value=\"Show Products\" onclick=\"showClientProducts()\" ></td>"
							+ "</tr>");
						}
					}
				%>
			</tbody>
		</table>
		</form>
		
		
		
		<form name="clients" action="" method="post">
			<table class="table table-striped table-bordered table-hover">
				<tbody>
					<%
						List<PropertyBond> bondProducts = (List<PropertyBond>) request.getSession().getAttribute("bondProducts");
						List<Investment> investmentProducts = (List<Investment>) request.getSession().getAttribute("investmentProducts");

						if (bondProducts != null && bondProducts.size() > 0) {
							out.println("<tr><th colspan=\"5\">Property bond</th></tr>" + "<tr>" + "<th>Principal Amount</th>"
									+ "<th>Interest Rate</th>" + "<th>Monthly Payment</th>" + "<th>Term</th>" + "<th>Total Payable</th>" + "</tr>");
							for (PropertyBond bond : bondProducts) {
								out.println("<tr>" + "<td>" + bond.getPrincipalAmount() + "</td>" + "<td>" + bond.getInterestRate() + "</td>"
										+ "<td>" + bond.getMonthlyPayment() + "</td>" + "<td>" + bond.getTerm() + "</td>" + "<td>"
										+ bond.getTotalPayable() + "</td>" + "</tr>");
							}
						}

						if (investmentProducts != null && investmentProducts.size() > 0) {
							out.println("<tr><th colspan=\"4\">Investment</th></tr>" + "<tr>" + "<th>Monthly Contibution</th>"
									+ "<th>Interest Rate</th>" + "<th>Term</th>" + "<th>Future Value</th>" + "</tr>");
							for (Investment investment : investmentProducts) {
								out.println("<tr>" + "<td>" + investment.getContribution() + "</td>" + "<td>" + investment.getInterestRate()
										+ "</td>" + "<td>" + investment.getTerm() + "</td>" + "<td>" + "no get method" + "</td>" + "</tr>");
							}
						}
					%>
				</tbody>
			</table>
		</form>
		
		
		<script type="text/javascript">
		function showClientProducts(){
			document.forms['clients'].action = '<%=request.getContextPath()%>/bond/clientproducts';
			document.forms['clients'].submit();
			
		}
	</script>
	
	<jsp:include page="../includes/footer.html" />

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../javascript/validation.js"></script>
	<script src="../vendor/jquery/jquery.min.js"></script>
	<script src="../vendor/tether/tether.min.js"></script>
	
</body>
</html>