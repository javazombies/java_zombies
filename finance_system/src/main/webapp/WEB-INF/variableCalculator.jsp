<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.javazombiez.finance.model.Investment"%>
<%@page import="com.javazombiez.finance.model.Event"%>
<%@page import="com.javazombiez.finance.utility.Money"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../bootstrap.css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style type="text/css">
table1 {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 80%;
	margin: auto;
}

td1, th1 {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr1:nth-child(even) {
	background-color: #dddddd;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Variable Calculator</title>
</head>
<body>

	<div style="float: right; width: 50%; text-align: center;">
		<%
			Investment investment = (Investment) request.getSession().getAttribute("investment");
		    
			request.setAttribute("header","Enter Base Investment");
			request.setAttribute("amountPlaceholder","monthly contribution");
		    request.setAttribute("termPlaceholder","investment term (months)");
		    request.setAttribute("interestPlaceholder","interest rate (%per annum)");
			
			request.setAttribute("amountLabel","Monthly Contribution");
			request.setAttribute("termLabel","Investment Term");
			request.setAttribute("interestLabel","Interest Rate");

			if (investment != null) {
			request.setAttribute("header","Enter Events");
		    request.setAttribute("amountPlaceholder","new monthly contribution");
		    request.setAttribute("termPlaceholder","from (month)");
		    request.setAttribute("interestPlaceholder","new interest rate (%per annum)");

			request.setAttribute("amountLabel","New Monthly Contribution");
			request.setAttribute("termLabel","Beginning Of Event");
			request.setAttribute("interestLabel","New Interest Rate");

			out.println("<h3>Investment Events</h3><hr>");
				out.println("<table class=\"table table-striped\" border=\"2\"><tr><th colspan=\"3\">Base Investment</th></tr><tr><th><u> Monthly Contribution </u></th> <th><u> Interest Rate </u></th> <th><u> Total Term </u></th></tr>");
				out.println("<tr><td> " + investment.getContribution() + "</td><td>" + investment.getInterestRate()
						+ "</td><td> " + investment.getTerm() + "</td></tr>");
				
				List<Event> events = investment.getEvents();

				if (events.size() > 0) {
				out.println("<tr><td colspan=\"3\"></td></tr><tr><th colspan=\"3\">Investment Events</th></tr><tr><th><u> Monthly Contribution </u></th> <th><u> Interest Rate </u></th> <th><u> From Month </u></th></tr>");

					for (int i = 0; i < events.size(); i++) {
						int month = events.get(i).getMonth();
						double interest = events.get(i).getInterestRate();
						Money contribution = events.get(i).getContribution();
						out.println("<tr><td>" + contribution + " </td><td> " + interest + " </td><td> " + month
								+ "</td></tr>");
					}
				}
				out.println("</table>");
			}
		%>
	</div>

	<div style="float: left; width: 50%; text-align: center;">
		<form name="eventsCalculator" action="" method="post">
			<h3><%=request.getAttribute("header")%></h3>
			<hr>
			<table class="table" style="width: 35%;" align="center">
				<tr>
					<td><label for="name" class="control-label" > <%=request.getAttribute("amountLabel") %> &nbsp;</label><input class="form-control" type="text" placeholder="<%=request.getAttribute("amountPlaceholder") %>" name="amount" /></td>
				</tr>
				<tr>
					<td><label for="name" class="control-label"><%=request.getAttribute("termLabel") %>&nbsp;</label><input class="form-control" type="text" placeholder="<%=request.getAttribute("termPlaceholder") %>" name="months" /></td>
				</tr>

				<tr>
					<td><label for="name" class="control-label"><%=request.getAttribute("interestLabel") %>&nbsp;</label><input class="form-control" type="text" placeholder="<%=request.getAttribute("interestPlaceholder") %>" name="interest" style="width: 394px; "/></td>
				</tr>
			</table>
						<input type="button" class="btn btn-default" value="Add Event" onclick="addEvent()" style="width: 130px; height: 41px" /> 
						<input type="button" class="btn btn-default" value="Refresh" onclick="refresh()" style="width: 130px; height: 41px" /> 
						<input type="button" class="btn btn-default" value="Calculate" onclick="calculate()" style="width: 130px; height: 41px" />
		</form>
	</div>
	<script type="text/javascript">
		function addEvent(){
			document.forms['eventsCalculator'].action = '<%=request.getContextPath()%>/investment/variableCalculator';
			document.forms['eventsCalculator'].submit();
		}
		function calculate(){
			document.forms['eventsCalculator'].action = '<%=request.getContextPath()%>/investment/variableCalculateEvents';
			document.forms['eventsCalculator'].submit();
		}
		
		function refresh(){
			document.forms['eventsCalculator'].action = '<%=request.getContextPath()%>/investment/refresh';
			document.forms['eventsCalculator'].submit();
		}
	</script>
</body>
</html>

