<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.tableCal {
	margin: 0px auto;
}
</style>
<link rel="stylesheet" type="text/css" href="../bootstrap.css" />
<title>Bond Calculator</title>
</head>

<body>
	<div class="container" style="text-align: center;">
		<div class="header" style="">
			<h2>Bond Calculator</h2>
			<hr>
		</div>
		<br /> <br />
		<div class="container">
			<div id="left-panel" style="float: left; width: 50%;">
				<form action="<%=request.getContextPath()%>/bond/bondCalculator"
					method="post">
					<div id="calculator">
						<table class="tableCal">
							<tr>
								<td><label> Purchase Amount <span><input
											value="0.0" class="form-control" type="text"
											placeholder="Purchase Amount" name="principalBondAmount" /></span>
								</label></td>
							</tr>
							<tr>
								<td><label> Deposit<span> <input value="0.0"
											class="form-control" type="text" placeholder="Deposit"
											name="deposit" /></span>
								</label></td>
							</tr>

							<tr>
								<td><label> Interest Rate <span> <input
											value="10.5" class="form-control" type="text"
											label="Interest Rate" placeholder="Interest Rate"
											name="interestRate" /></span>
								</label></td>
							</tr>
							<tr>
								<td><label> Term <span> <input value="240"
											class="form-control" type="text" placeholder="Term"
											name="term" /></span>
								</label></td>
							</tr>
						</table>
						<!-- table ends -->
						<div>
							<br /> <input type="submit" class="btn btn-default"
								value="Calculate" style="width: 20%;" />
						</div>
					</div>
					<br /> <br />


				</form>
			</div>
			<div id="right-panel"
				style="width: 50%; float: right; text-align: left;">
				<div>
					<br />
					<p>
						<u>Bond Parameters</u>
					</p>
					<hr />
				</div>
				<table id="bondParameters" class="table">
					<thead>
						<tr>
							<th>Principal Amount</th>
							<th>Interest Rate</th>
							<th>Deposit Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${param_amount}</td>
							<td>${param_interest}</td>
							<td>${param_deposit}</td>
						</tr>
					</tbody>
				</table>
				<div>
					<br />
					<p>
						<u>Calculation Results</u>
					</p>
					<hr />
				</div>

				<table id="bondResults" class="table table-striped">
					<thead>
						<tr>
							<th>Monthly Payment</th>
							<th>Total Payment</th>
							<th>Total Interest</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${monthlyPayment}</td>
							<td>${totalPayment}</td>
							<td>${totalInterest}</td>
						</tr>
					</tbody>
				</table>
				<div>
					<br />
					<p>
						<u>Estimated Costs</u>
					</p>
					<hr />
				</div>
				<table id="bondCosts" class="table table-striped">
					<thead>
						<tr>
							<th>Registration Fee</th>
							<th>Legal Fees</th>
							<th>Transfer Costs</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${registrationFee}</td>
							<td>${legalFees}</td>
							<td>${tranferCosts}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div id="buttonDiv">
			<br /> <input type="submit" class="btn btn-default"
				value="View Forecast" style="width: 20%;" disabled="true" />
		</div>

	</div>



</body>
</html>