<%@page import="com.javazombiez.finance.model.*"%>
<%@page import="java.util.*"%>
<%@page import="com.javazombiez.finance.controller.*"%>
<%@page import="com.javazombiez.finance.utility.Money"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html id="htmlInvest">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Custom css -->
<link href="../css/styles1.css" rel="stylesheet">
<link href="../css/styles2.css" rel="stylesheet">
<link href="../css/styles3.css" rel="stylesheet">
<link rel="icon" href="../img/penny.ico">
<!-- Bootstrap -->
<!-- <link href="../css/bootstrap.min.css" rel="stylesheet"> -->
<link href="../css/index-page.css" rel="stylesheet">
<link href="../vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic"
	rel="stylesheet" type="text/css">
<link href="../css/java_zombies.css" rel="stylesheet">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style type="text/css">
.money {
	text-align: right;
}

.investment {
	width: 100px;
}
</style>

<title>investment Calculator</title>
</head>
<body id="bodyInvest">

	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<h1 style="text-align: center">Investment Calculator</h1>
				<div>&nbsp;</div>
			</div>
			<hr>
		</div>


		<div id="left-panel" style="float: left; width: 30%;">

			<div class="panel panel-success">
				<div class="panel-heading HeadingOverride">Investment Details</div>
				<div class="panel-body">
					<form name="investmentCalculator"
						action="<%=request.getContextPath()%>/investment/calculator"
						method="post" onsubmit="return validateInvestmentForm()">

						<div style="text-align: left;">
							<label>Fund Option</label><select class="form-contol"
								name="investmentFund" style="float: right; height: 15%;">
								<option value="Money Market">Money Market</option>
								<option value="Tax Free Investment">Tax Free Investment</option>
								<option value="Equity Fund">Equity Fund</option>
							</select>
						</div>

						<div>&nbsp;</div>
						<div class="form-group">
							<label for="contributionAmount" class="control-label">Monthly
								Contribution </label>
							<div class="input-group">
								<span class="input-group-addon ResultsTextOverride">R</span> <input
									class="form-control" data-type="money" id="monthlyContribution"
									maxlength="15" name="monthlyContribution" type="text"
									value="${monthlyContribution}" />
							</div>
						</div>
						<div class="form-group">
							<label for="investmentTerm" class="control-label">Investment
								Term</label>
							<div class="input-group">
								<span class="input-group-addon ResultsTextOverride">M</span> <input
									class="form-control" id="investmentTerm" name="investmentTerm"
									type="text" value="${investmentTerm}" />
							</div>
						</div>
						<div class="form-group">
							<label for="Rate" class="control-label">Interest Rate </label>
							<div class="input-group">
								<span class="input-group-addon ResultsTextOverride">%</span> <input
									class="form-control" data-type="money" id="rate" maxlength="15"
									name="rate" type="text" value="${rate}" />
							</div>
						</div>

						<div id="error-div" hidden="true"
							class="alert alert-danger fade in alert-white rounded js_P24_BondCalculatorsErrors panel">
						</div>

						<input name="action" type="submit" value="Calculate"
							class="btn btn-success" />
						<button type="button" class="btn btn-success"
							data-toggle="collapse" data-target="#events">Add events</button>
						<input name="action" type="submit" value="implement"
							class="btn btn-success" />
						<div id="events" class="collapse table-responsive">
							<br> <br>

							<table>
								<tbody>
									<tr>
										<th>Event</th>
										<th>From Month</th>
										<th>Contribution</th>
										<th>Investment Rate</th>
									</tr>
									<tr>
										<td>1</td>
										<td><input class="investment" name="eventTerm1"
											value="${eventTerm1}"></td>
										<td><input class="investment" name="eventContribution1"
											value="${eventContribution1}"></td>
										<td><input class="investment" name="eventRate1"
											value="${eventRate1}"></td>
									</tr>
									<tr>
										<td>2</td>
										<td><input class="investment" name="eventTerm2"
											value="${eventTerm2}"></td>
										<td><input class="investment" name="eventContribution2"
											value="${eventContribution2}"></td>
										<td><input class="investment" name="eventRate1"
											value="${eventRate2}"></td>
									</tr>
									<tr>
										<td>3</td>
										<td><input class="investment" name="eventTerm3"
											value="${eventTerm3}"></td>
										<td><input class="investment" name="eventContribution3"
											value="${eventContribution3}"></td>
										<td><input class="investment" name="eventRate3"
											value="${eventRate3}"></td>
									</tr>
									<tr>
										<td>4</td>
										<td><input class="investment" name="eventTerm4"
											value="${eventTerm4}"></td>
										<td><input class="investment" name="eventContribution4"
											value="${eventContribution4}"></td>
										<td><input class="investment" name="eventRate4"
											value="${eventRate4}"></td>
									</tr>
									<tr>
										<td>5</td>
										<td><input class="investment" name="eventTerm5"
											value="${eventTerm5}"></td>
										<td><input class="investment" name="eventContribution5"
											value="${eventContribution5}"></td>
										<td><input class="investment" name="eventRate5"
											value="${eventRate5}"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div id="right-panel" style="float: right; width: 65%;">
			<div class="row">
				<div class="col-xs-11">
					<div class="panel panel-success">
						<div class="panel-heading">Base Investment</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-7">Monthly Contribution</div>
								<div class="col-xs-5 text-primary">${param_monthlyContribution}</div>
							</div>
							<div class="row">
								<div class="col-xs-7">Investment Term</div>
								<div class="col-xs-5 text-primary">${param_investmentTerm}</div>
							</div>
							<div class="row">
								<div class="col-xs-7">Interest Rate</div>
								<div class="col-xs-5 text-primary">${param_rate}%</div>
							</div>
						</div>
					</div>

				</div>
			</div>



			<div class="row">
				<div class="col-xs-11">
					<div class="table-responsive">

						<table id="investTable"
							class="table table-striped table-bordered table-hover money">

							<tbody>
								<tr>
									<th>Month</th>
									<th>Opening Balance</th>
									<th>Monthly Contribution</th>
									<th>Interest Rate</th>
									<th>Interest Earned</th>
									<th>Closing Balance</th>
								</tr>


								<%
								  if (request.getAttribute("forecast") != null) {
									Forecast forecast = (Forecast) request.getAttribute("forecast");
									List<ForecastItem> items = forecast.getForcastItems();
									for (int i = 0; i < items.size(); i++) {
									  out.println("<tr><td>" + items.get(i).getMonth() + "</td>" + "<td>" + items.get(i).getOpeningBalance()
									      + "</td>" + "<td>" + items.get(i).getContribution() + "</td>" + "<td>" + items.get(i).getInterestRate()
									      + " </td>" + "<td>" + items.get(i).getInterestEarned() + "</td>" + "<td>"
									      + items.get(i).getClosingBalance() + "</td>" + "</tr>");
									}
								  }
								%>

							</tbody>
						</table>
					</div>
				</div>
			</div>


		</div>

	</div>
	<jsp:include page="../includes/footer.html" />
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/validation.js"></script>
	<script src="../vendor/jquery/jquery.min.js"></script>
	<script src="../vendor/tether/tether.min.js"></script>
</body>
</html>