<%@page import="com.javazombiez.finance.model.BondForecastItem"%>
<%@page import="java.util.*"%>
<%@page import="com.javazombiez.finance.model.PropertyBond"%>
<%@page import="com.javazombiez.finance.utility.Money"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html id="htmlBond">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Bond Calculator</title>
<!-- Custom css -->
<link href="../css/styles1.css" rel="stylesheet">
<link href="../css/styles2.css" rel="stylesheet">
<link href="../css/styles3.css" rel="stylesheet">


<!-- Bootstrap -->
<!-- <link href="../css/bootstrap.css" rel="stylesheet"> -->
<link rel="icon" href="../img/penny.ico">
<!-- <link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet"> -->
<link href="../css/index-page.css" rel="stylesheet">
<link href="../vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic"
	rel="stylesheet" type="text/css">
<!-- Custom css -->
<link href="../css/java_zombies.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style type="text/css">
.re {
	width: 100px;
}
</style>
</head>
<body id="bodyBond">
<%-- 			<jsp:include page="../includes/nav-bar.html" /> --%>
	<div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 ">
					<h1 style="text-align: center">Bond Calculator</h1>
					<div>&nbsp;</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="panel panel-success">
						<div class="panel-heading HeadingOverride">Bond Details</div>
						<div class="panel-body">
							<form name="bondCalculator"
								action="<%=request.getContextPath()%>/bond/bondCalculator"
								method="post" onsubmit="return validateForm()">
								<div class="form-group">
									<label for="Loan" class="control-label">Purchase Amount</label>
									<div class="input-group">
										<span class="input-group-addon ResultsTextOverride">R</span> <input
											class="form-control" data-type="money" id="Loan"
											maxlength="15" name="loan" type="text" value="${loan}" />
									</div>
								</div>
								<div class="form-group">
									<label for="Rate" class="control-label">Interest Rate</label>
									<div class="input-group">
										<span class="input-group-addon ResultsTextOverride">%</span> <input
											class="form-control" id="rate" name="rate" type="text"
											value="${rate}" />
									</div>
								</div>
								<div class="form-group">
									<label for="Deposit" class="control-label">Deposit
										Amount</label>
									<div class="input-group">
										<span class="input-group-addon ResultsTextOverride">R</span> <input
											class="form-control" data-type="money" id="Deposit"
											maxlength="15" name="deposit" type="text"
											value="${depositInput}" />
									</div>
								</div>
								<div class="form-group">
									<label for="Years" class="control-label">Number of
										Months</label>
									<div class="input-group">
										<span class="input-group-addon ResultsTextOverride">M</span> <input
											class="form-control" id="term" name="term" type="text"
											value="${term}" />
									</div>
								</div>
								<div id="error-div" hidden="true" class="alert alert-danger fade in alert-white rounded js_P24_BondCalculatorsErrors panel">
       
								</div>
								<button type="button" class="btn btn-success"
									data-toggle="collapse" data-target="#events">show
									events</button>
								<input name="action" type="submit" value="calculate"
									class="btn btn-success" /> <input name="action" type="submit"
									value="implement" class="btn btn-success" />
								<div id="events" class="collapse table-responsive">
									<br> <br>
									<table class="eventTable">
										<tbody>
											<tr>
												<th>Event#</th>
												<th>Month</th>
												<th>Interest</th>
												<th>Payment</th>
											</tr>
											<tr>
												<td>1</td>
												<td><input class="re" name="eventMonth1"
													value="${eventMonth1}" width="10px"></td>
												<td><input class="re" name="eventInterest1"
													value="${eventInterest1}"></td>
												<td><input class="re" name="eventPayment1"
													value="${eventPayment1}"></td>
											</tr>
											<tr>
												<td>2</td>
												<td><input class="re" name="eventMonth2"
													value="${eventMonth2}" width="10px"></td>
												<td><input class="re" name="eventInterest2"
													value="${eventInterest2}"></td>
												<td><input class="re" name="eventPayment2"
													value="${eventPayment2}"></td>
											</tr>
											<tr>
												<td>3</td>
												<td><input class="re" name="eventMonth3"
													value="${eventMonth3}"></td>
												<td><input class="re" name="eventInterest3"
													value="${eventInterest3}"></td>
												<td><input class="re" name="eventPayment3"
													value="${eventPayment3}"></td>
											</tr>
											<tr>
												<td>4</td>
												<td><input class="re" name="eventMonth4"
													value="${eventMonth4}"></td>
												<td><input class="re" name="eventInterest4"
													value="${eventInterest4}"></td>
												<td><input class="re" name="eventPayment4"
													value="${eventPayment4}"></td>
											</tr>
											<tr>
												<td>5</td>
												<td><input class="re" name="eventMonth5"
													value="${eventMonth5}"></td>
												<td><input class="re" name="eventInterest5"
													value="${eventInterest5}"></td>
												<td><input class="re" name="eventPayment5"
													value="${eventPayment5}"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<div class="panel panel-success">
						<div class="panel-heading">Bond Details</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-5">Principal Amount</div>
								<div class="col-xs-7 text-primary money">${purchaseAmount}</div>
								<span></span>
							</div>
							<div class="row">
								<div class="col-xs-5">Deposit Amount</div>
								<div class="col-xs-7 text-primary money">${deposit}</div>
							</div>
							<div class="row">
								<div class="col-xs-5">Interest Rate</div>
								<div class="col-xs-7 text-primary money">${rate}%</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<div class="panel panel-success">
						<div class="panel-heading">Base Bond Results</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-7 ">Monthly Payment</div>
								<div class="col-xs-5 text-primary money">${monthlyPayment}</div>
							</div>
							<div class="row">
								<div class="col-xs-7">Total Payment</div>
								<div class="col-xs-5 text-primary money">${totalPayment}</div>
							</div>
							<div class="row">
								<div class="col-xs-7">Total Interest</div>
								<div class="col-xs-5 text-primary money">${totalInterest}</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<div class="panel panel-success">
						<div class="panel-heading">Event Results</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-7">Total Savings</div>
								<div class="col-xs-5 text-primary money">0</div>
							</div>
							<div class="row">
								<div class="col-xs-7">New Loan Duration</div>
								<div class="col-xs-5 text-primary money">0 months</div>
							</div>
							<div class="row">
								<div class="col-xs-12">&nbsp;</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<div class="panel panel-success">
						<div class="panel-heading">Costs</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-7">Bond Cost</div>
								<div class="col-xs-5 text-primary money">${registrationFee}</div>
							</div>
							<div class="row">
								<div class="col-xs-7">Transfer Cost</div>
								<div class="col-xs-5 text-primary money">${tranferCosts}</div>
							</div>
							<div class="row">
								<div class="col-xs-7">Legal Fees</div>
								<div class="col-xs-5 text-primary money">${legalFees}</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-8">
					<div class="table-responsive">
						<table id="bondTable" class="table table-striped table-bordered table-hover">
							<tbody>
								<tr>
									<th>Payment #</th>
									<th>Payment</th>
									<th>Interest</th>
									<th>Loan Reduction</th>
									<th>Balance</th>
								</tr>
								<tr>
									<td>0</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td class="money">${purchaseAmount}</td>
								</tr>

								<%
								  if( request.getAttribute( "bondForecastItems" ) != null ) {
									List< BondForecastItem > bondForecastItems = (List< BondForecastItem >) request
									    .getAttribute( "bondForecastItems" );
									for ( BondForecastItem item : bondForecastItems ) {
									  out.println(
									      "<tr><td width=\"10\">" + item.getMonth() + "</td>" + "<td class=\"money\">" + item.getMonthlyPayment()
										  + "</td>" + "<td class=\"money\">" + item.getInterest() + "</td>" + "<td class=\"money\">"
										  + item.getLoanReduction() + "</td>" + "<td class=\"money\">" + item.getBalance() + "</td></tr>" );
									}
								  }
								%>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>


	<jsp:include page="../includes/footer.html" />

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../js/bootstrap.min.js"></script>

	<script type="text/javascript" src="../javascript/validation.js"></script>
	<script src="../vendor/jquery/jquery.min.js"></script>
	<script src="../vendor/tether/tether.min.js"></script>


</body>
</html>