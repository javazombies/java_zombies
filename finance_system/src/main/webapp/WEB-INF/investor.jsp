<%@page import="com.javazombiez.finance.model.Investment"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../bootstrap.css" />
<title>create investor</title>
<style type="text/css">
.table {
	margin: 0px auto;
}
form {
	font-family: arial, sans-serif;
	width="60%";
}
</style>
</head>

<body>
	<div class="container">
		<%
		  Investment investment = (Investment) request.getSession().getAttribute( "investment" );
		%>
	
		<h2 align="center">Selected Product</h2>
		<table class="table">
			<%
			  out.println( "<tr><td>Regular monthly contribution</td><td>R" + investment.getContribution() + "</td></tr>" );
			  out.println( "<tr><td>Interest rate earned annually</td><td>" + investment.getInterestRate() + "%</td></tr>" );
			  
			%>
		</table>
		<hr />
		<h3>Existing Client</h3>
		<h4>(Enter ID number to add investment on an existing client)</h4>
		<label>ID Number: </label><input class="form-control" type='number' 
							 name='identity_number' required="true" /><br>
		
		<input class="btn btn-default" type="submit"
					value="Add investment" style="width: 15%;" />
		<hr />
		<h3>Add Investment Client</h3>
		<div style="text-align: center;">
			<form align="center" class="form" action="<%=request.getContextPath()%>/investor/add"
				method="post">
					 <div>
						<label >First Name:</label><input class="form-control" type="text" 
							 name="first_name" required="true" />
					</div>

					<label>Last Name:</label><input class="form-control" type='text' 
							 name='surname' required /><br>
					
										
					<label>Date Of Birth:</label><input class="form-control" type='date' 
							 name='date_of_birth' required="true" /><br>
					

					
				    <label>ID Number: </label><input class="form-control" type='number' 
							 name='identity_number' required="true" /><br>
							
					
										
					<label>Gender: </label><select class="form-control" name="gender" >
									<option value="Male" selected="selected">Male</option>
									<option value="Female">Female</option>
									</select>
									<br>		

					<label>Phone Number: </label><input class="form-control" type='number' 
						name='contact_number' required /><br>
							
					
					
					<label>Email: </label><input class= "form-control" type="email" 
					    name ='email' required/><br>
					   
					
					<label>Tax Number:</label> <input class="form-control" type='number' label="Tax Number"
						 name='tax_number' required="true" /><br>	
				<br /> <br /> <input class="btn btn-default" type="submit"
					value="create investor" style="width: 15%;" />
			</form>
			
			
			
		</div>
	</div>
</body>
</html>