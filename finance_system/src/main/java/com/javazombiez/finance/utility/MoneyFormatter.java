package com.javazombiez.finance.utility;

/**
 * Formats a double to be displayed the way money should be displayed
 * 
 * @author Teddy
 *
 */
public class MoneyFormatter {

  public static String format( String money ) {
    String[] randsAndCents = ( money + "" ).split( "\\." );
    String currencyUnits = randsAndCents[0];
    String cents;
    // Currency units formatting
    if( currencyUnits.length() > 3 ) {
      int counter = 0;
      String temp = "";
      // 1_213_896.00
      for ( char unit : reverseCharArray( currencyUnits.toCharArray() ) ) {
	if( counter % 3 == 0 && counter != 0 )
	  temp += ",";
	temp += unit;
	counter++;
      }
      currencyUnits = new String( reverseCharArray( temp.toCharArray() ) );
    }
    // Cents formatting
    if( randsAndCents.length > 1 ) {
      cents = randsAndCents[1];
      if( cents.length() < 2 )
	cents += "0";
    }
    else {
      cents = "00";
    }
    return currencyUnits + "." + cents;
  }

  private static char[] reverseCharArray( char[] array ) {
    for ( char i = 0 ; i < array.length / 2 ; i++ ) {
      char temp = array[i];
      array[i] = array[array.length - i - 1];
      array[array.length - i - 1] = temp;
    }
    return array;
  }
}
