package com.javazombiez.finance.utility;
/**
 * 
 * @author Teboho
 *
 */
public final class TaxUtil {

	public static final Money calculateIncomeTax(double grossSalary) {
		double[] brackets = { 6312, 15823.33, 24711.66, 34205, 46300, 59025.83, 125000 };
		double[] percentage = { 0.18, 0.26, 0.31, 0.36, 0.39, 0.41, 0.45 };

		double totalTax = 0;
		double taxable = 0;
		double UIF = 0;
		double differenceSalary = grossSalary;

		if (0.01 * grossSalary > 148.72) {
			UIF = 148.72;
		}
		else {
			UIF = 0.01 * grossSalary;
		}

		for (int i = brackets.length - 1; i >= 0; i--) {
			if (differenceSalary > brackets[i]) {
				taxable = differenceSalary - brackets[i];
				totalTax += taxable * percentage[i];
				differenceSalary -= taxable;
			}
		}
		double nettSalary = grossSalary - totalTax - UIF;

		return new Money(nettSalary);
	}

	public static final Money calculateTransferDuty(double valueOfProperty) {

		double[] brackets = { 900_000, 1_250_000, 1_750_000, 2_250_000, 10_000_000 };
		double[] percentage = {0.03, 0.06, 0.08, 0.11, 0.13 };

		double transferDuty = 0;
		double taxable = 0;
		double differenceValue = valueOfProperty;

		for (int i = brackets.length - 1; i >= 0; i--) {
			if (differenceValue > brackets[i]) {
				taxable = differenceValue - brackets[i];
				transferDuty += taxable * percentage[i];
				differenceValue -= taxable;
			}
		}

		return new Money(transferDuty);
	}

}
