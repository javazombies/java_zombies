package com.javazombiez.finance.utility;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.persistence.Embeddable;

/**
 * Represents money.
 * 
 * @author Teddy
 *
 */
@Embeddable
public class Money {

  private BigDecimal value;

  public Money ( double money ) {
    value = new BigDecimal( money );
  }

  public Money ( BigDecimal money ) {
    value = money;
  }

  public Money ( ) {
    value = BigDecimal.ZERO;
  }

  /**
   * Adds Money
   * 
   * @param Money
   *          to be added
   * @return the results which is a new money object
   */
  public Money add( Money money ) {
    return new Money( getValue().add( money.getValue() ) );
  }

  /**
   * Subtracts money
   * 
   * @param money
   *          to be subtracted
   * @return the results which is a new money object
   */
  public Money subtract( Money money ) {
    return new Money( getValue().subtract( ( money.getValue() ) ) );
  }

  /**
   * Multiplies money with the money parameter
   * 
   * @param money
   *          to multiply with
   * @return the results which is a new money object
   */
  public Money multiply( Money money ) {
    return new Money( getValue().multiply( money.getValue() ) );
  }

  /**
   * Multiplies money with the double parameter
   * 
   * @param double
   *          to multiply with
   * @return the results which is a new money object
   */
  public Money multiply( double money ) {
    return multiply( new Money( money ) );
  }

  /**
   * Divides money with the money parameter
   * 
   * @param money
   *          denominator
   * @return the results which is a new money object
   */
  public Money divide( Money money ) {
    return new Money( getValue().divide( money.getValue(), MathContext.DECIMAL128 ) );
  }

  /**
   * Returns the value of the money
   * 
   * @return BigDecimal representing the value of the money
   */
  public BigDecimal getValue() {
    return value.setScale( 2, BigDecimal.ROUND_HALF_UP );
  }

  public double getValueAsDouble() {
    return Double.parseDouble( value.setScale( 2, BigDecimal.ROUND_HALF_UP ).toString() );
  }

  @Override
  public boolean equals( Object obj ) {
    if( this == obj )
      return true;
    if( obj == null )
      return false;
    if( ! ( obj instanceof Money ) )
      return false;
    Money money = (Money) obj;
    return this.toString().equals( money.toString() );
  }

  @Override
  public String toString() {
    return MoneyFormatter.format( value.setScale( 2, BigDecimal.ROUND_HALF_UP ).toString() );
  }
}
