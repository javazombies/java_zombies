package com.javazombiez.finance.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.javazombiez.finance.model.BusinessProduct;
import com.javazombiez.finance.model.Investor;
import com.javazombiez.finance.model.PropertyBond;

@Named
@RequestScoped
public class InvestorResource {

	@PersistenceContext(unitName = "primary")
	EntityManager entityManager;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addInvestor(Investor investor) {
		generateUniqueDomainKey(investor);
		entityManager.persist(investor);
	}

	/**
	 * There were problems with Hibernate auto-Generation of non-id columns.
	 * 
	 * @param investor
	 */
	private void generateUniqueDomainKey(Investor investor) {
		List<Investor> investors = getListOfInvestors();
		if (!investors.isEmpty())
			investor.setInvestorNumber(investors.get(investors.size() - 1).getInvestorNumber() + 1);
		else {
			investor.setInvestorNumber(1);
		}
	}

	/**
	 * returns a list of all existing investros
	 * 
	 * @return
	 */
	public List<Investor> getListOfInvestors() {
		// alternative could be to use the sql 'MAX()' function and get only one
		// Investor.
		List<Investor> investors = entityManager.createQuery("SELECT i FROM Investor i", Investor.class).getResultList();
		return investors;
	}

	public Investor getInvestor(int idNumber) {
		Investor investor = entityManager
				.createQuery("SELECT i FROM Investor i WHERE i.identityNumber = " + idNumber, Investor.class).getSingleResult();
		return investor;
	}

	public List<BusinessProduct> getInvestorProducts(long investorNumber) {
		List<BusinessProduct> products = new ArrayList<>();
		Investor investor = (Investor) entityManager.createQuery("SELECT i FROM Investor i  WHERE i.investorNumber = " + investorNumber, Investor.class).getSingleResult();
		int id = investor.getId();
		List<BusinessProduct> bondProducts = (List<BusinessProduct>) entityManager.createQuery("SELECT i FROM PropertyBond i WHERE investor_id = " + id, BusinessProduct.class).getResultList();
		List<BusinessProduct> investmentProducts = (List<BusinessProduct>) entityManager.createQuery("SELECT i FROM  Investment i WHERE investor_Id = " + id, BusinessProduct.class).getResultList();
		if (bondProducts != null) {
			products.addAll(bondProducts);
		}
		if (investmentProducts != null) {
			products.addAll(investmentProducts);
		}
		return products;
	}

}
