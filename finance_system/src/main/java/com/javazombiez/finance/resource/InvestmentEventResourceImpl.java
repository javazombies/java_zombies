package com.javazombiez.finance.resource;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.javazombiez.finance.model.Event;
import com.javazombiez.finance.model.Investment;

/**
 * 
 * @author Teboho
 *
 */
public class InvestmentEventResourceImpl implements InvestmentEventResource {

	@PersistenceContext(unitName = "primary")
	EntityManager entityManager;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addEvents(List<Event> events) {
		for (Event event : events) {
			entityManager.persist(event);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Event> getEvents(int investmentId) {
		Investment investment = entityManager.find(Investment.class, investmentId);
		List<Event> events = investment.getEvents();
		return events;
	}

}
