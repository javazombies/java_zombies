package com.javazombiez.finance.resource;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.javazombiez.finance.model.BondEvent;
import com.javazombiez.finance.model.PropertyBond;

/**
 * 
 * @author Teboho
 *
 */
@Named
@RequestScoped
public class BondResourceImpl implements BondResource {

	@PersistenceContext(unitName = "primary")
	EntityManager entityManager;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addBond(PropertyBond bond) {
		List<BondEvent> events = bond.getBondEvents();
		for (BondEvent event : events) {
			entityManager.persist(event);
		}
		entityManager.persist(bond);
	}
}