package com.javazombiez.finance.resource;

import java.util.List;

import com.javazombiez.finance.model.Event;

/**
 * 
 * @author Teboho
 *
 */
public interface InvestmentEventResource {

	public abstract void addEvents(List<Event> events);

	public abstract List<Event> getEvents(int investmentId);
}
