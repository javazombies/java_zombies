package com.javazombiez.finance.resource;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.javazombiez.finance.model.Event;
import com.javazombiez.finance.model.Investment;
import com.javazombiez.finance.model.InvestmentFund;
import com.javazombiez.finance.model.InvestmentProduct;

@Named
@RequestScoped
public class InvestmentResourceImpl implements InvestmentResource {

	@PersistenceContext(unitName = "primary")
	EntityManager entityManager;

	@Override
	public void createInvestment(Investment investment) {
		InvestmentProduct investmentProduct = new InvestmentProduct(investment.getInvestmentFund());
		investmentProduct.setInvestment(investment);// FK
		List<Event> events = investment.getEvents();
		for (Event event : events) {
			entityManager.persist(event);
		}
		entityManager.persist(investment);
		entityManager.persist(investmentProduct);
//		persistEvents(investment);
	}

	/**
	 * persists all events related to this investment
	 * 
	 * @param investment
	 */
	private void persistEvents(Investment investment) {
		List<Event> listOfEvents = investment.getEvents();
		int jdbBatchSize = 50;
		for (int i = 0; i < listOfEvents.size(); i++) {
			entityManager.persist(listOfEvents.get(i));
			if (i % jdbBatchSize == 0) {
				entityManager.flush();
				entityManager.clear();
			}
		}
		// clean up for the next persist.
		entityManager.flush();
		entityManager.clear();
	}

	@Override
	public List<Investment> retrieveInvestments() {
		List<Investment> investments = entityManager.createQuery("SELECT i FROM Investment i", Investment.class)
				.getResultList();
		return investments;
	}

	@Override
	public Investment retrieveInvestment(int id) {
		return entityManager.find(Investment.class, id);
	}
}
