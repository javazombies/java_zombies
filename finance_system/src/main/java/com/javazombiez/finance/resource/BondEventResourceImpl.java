package com.javazombiez.finance.resource;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.javazombiez.finance.model.BondEvent;
import com.javazombiez.finance.model.PropertyBond;

public class BondEventResourceImpl implements BondEventResource {

	@PersistenceContext(unitName = "primary")
	EntityManager entityManager;

	@Override
	public void addEvents(List<BondEvent> events) {
		for (BondEvent bondEvent : events) {
			entityManager.persist(bondEvent);
		}
	}

	@Override
	public List<BondEvent> getEvents(int bondId) {
		PropertyBond bond = entityManager.find(PropertyBond.class, bondId);
		List<BondEvent> events = bond.getBondEvents();
		return events;
	}

}
