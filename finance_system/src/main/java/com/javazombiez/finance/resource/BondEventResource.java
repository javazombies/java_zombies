package com.javazombiez.finance.resource;

import java.util.List;

import com.javazombiez.finance.model.BondEvent;
import com.javazombiez.finance.model.PropertyBond;

/**
 * 
 * @author Teboho
 *
 */

public interface BondEventResource {

	public abstract void addEvents(List<BondEvent> events);

	public abstract List<BondEvent>  getEvents(int bondId);
}
