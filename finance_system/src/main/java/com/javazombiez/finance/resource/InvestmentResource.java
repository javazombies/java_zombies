package com.javazombiez.finance.resource;

import java.util.List;

import com.javazombiez.finance.model.Investment;

/**
 * Responsible for the persistence of all the investments
 *
 */
public interface InvestmentResource {

	/**
	 * Inserts the investment into the investment table.
	 */
	void createInvestment(Investment investment);

	/**
	 * Retrieves the list of all available investments.
	 * 
	 * @return List of all investments
	 */
	List<Investment> retrieveInvestments();

	/**
	 * Takes in the specific id and returns the Investment corresponding to that
	 * id.
	 * 
	 * @param id
	 * @return
	 */
	Investment retrieveInvestment(int id);
}
