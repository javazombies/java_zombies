package com.javazombiez.finance.resource;

import javax.ejb.Local;

import com.javazombiez.finance.model.PropertyBond;

/**
 * 
 * @author Teboho
 *
 */
@Local
public interface BondResource {

	public abstract void addBond(PropertyBond bond);
}
