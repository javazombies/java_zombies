package com.javazombiez.finance.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.javazombiez.finance.utility.Money;

/**
 * 
 * @author Teddy
 *
 */
public class Forecast {

  private Investment investment;

  private List< ForecastItem > forcastItems = new ArrayList<>();

  public Forecast ( Investment investment ) {
    this.investment = investment;
  }

  public Money getFutureValue() {
    List< ForecastItem > forcastItems = getForcastItems();
    return forcastItems.get( forcastItems.size() - 1 ).getClosingBalance();
  }

  public List< ForecastItem > getForcastItems() {
    /*
     * if( investment.getEvents().isEmpty() ) return modelOneForcast(); return modelTwoForcast();
     */
    Map< Integer,Event > events = null;
    if( ! investment.getEvents().isEmpty() ) {
      events = mapEvents();
    }
    Money openingBalance = new Money( 0 );
    Money contribution = investment.getContribution();
    Money interestEarned = new Money( 0 );
    Money closingBalance = new Money( 0 );
    double interestRate = investment.getInterestRate();
    double interestRatePerMonth = ( interestRate / 100.0 ) / 12.0;
    for ( int i = 1 ; i <= investment.getTerm() ; i++ ) {
      openingBalance = closingBalance;
      if( events != null && events.get( i ) != null ) {
	contribution = ( events.get( i ).getContribution() != null ) ? events.get( i ).getContribution() : contribution;
	interestRate = ( events.get( i ).getInterestRate() != 0 ) ? events.get( i ).getInterestRate() : interestRate;
      }
      interestRatePerMonth = ( interestRate / 100.0 ) / 12.0;
      interestEarned = openingBalance.add( contribution ).multiply( interestRatePerMonth );
      closingBalance = openingBalance.add( interestEarned ).add( contribution );
      forcastItems
	  .add( new ForecastItem( i, openingBalance, contribution, interestRate, interestEarned, closingBalance ) );
    }
    return forcastItems;
  }

  private Map< Integer,Event > mapEvents() {
    Map< Integer,Event > events = new HashMap< Integer,Event >();
    for ( Event e : investment.getEvents() ) {
      events.put( e.getMonth(), e );
    }
    return events;
  }
}
