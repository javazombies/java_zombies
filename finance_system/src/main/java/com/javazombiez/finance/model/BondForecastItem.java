package com.javazombiez.finance.model;

import com.javazombiez.finance.utility.Money;

public class BondForecastItem {

  private int month;

  private Money monthlyPayment;

  private Money interest;

  private Money loanReduction;

  private Money balance;

  public BondForecastItem ( int month, Money monthlyPayment, Money interest, Money loanReduction, Money balance ) {
    super();
    this.month = month;
    this.monthlyPayment = monthlyPayment;
    this.interest = interest;
    this.loanReduction = loanReduction;
    this.balance = balance;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth( int month ) {
    this.month = month;
  }

  public Money getMonthlyPayment() {
    return monthlyPayment;
  }

  public void setMonthlyPayment( Money monthlyPayment ) {
    this.monthlyPayment = monthlyPayment;
  }

  public Money getInterest() {
    return interest;
  }

  public void setInterest( Money interest ) {
    this.interest = interest;
  }

  public Money getLoanReduction() {
    return loanReduction;
  }

  public void setLoanReduction( Money loanReduction ) {
    this.loanReduction = loanReduction;
  }

  public Money getBalance() {
    return balance;
  }

  public void setBalance( Money balance ) {
    this.balance = balance;
  }

  @Override
  public String toString() {
    return "BondForecastItem [month=" + month + ", monthlyPayment=" + monthlyPayment + ", interest=" + interest
	+ ", loanReduction=" + loanReduction + ", balance=" + balance + "]";
  }
  
}
