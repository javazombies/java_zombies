package com.javazombiez.finance.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.javazombiez.finance.utility.Money;

public class BondForecast {

  private PropertyBond bond;

  private List< BondForecastItem > bondForcastItems = new ArrayList<>();

  private int newTerm;

  public BondForecast ( PropertyBond bond ) {
    this.bond = bond;
  }

  public List< BondForecastItem > getBondForcastItems() {
    /*
     * if( investment.getEvents().isEmpty() ) return modelOneForcast(); return modelTwoForcast();
     */
    Map< Integer,BondEvent > events = null;
    if( ! bond.getBondEvents().isEmpty() ) {
      events = mapEvents();
    }
    double interestRate = ( bond.getInterestRate() / 100.0 ) / 12.0;
    Money monthlyPayment = this.bond.getMonthlyPayment();
    Money interest = new Money( 0 );
    Money loanReduction = new Money( 0 );
    Money balance = bond.getPrincipalAmount();
    for ( int month = 1 ; month <= bond.getTerm() ; month++ ) {
      if( events != null && events.get( month ) != null ) {
	if( events.get( month ).getInterestRate() != 0 ) {
	  interestRate = ( events.get( month ).getInterestRate() / 100.0 ) / 12.0;
	  monthlyPayment = getNewMonthlyPayment( balance, interestRate, bond.getTerm() - month + 1 );
	}
	monthlyPayment = ( events.get( month ).getMonthlyPayment() != null
	    && ( ( events.get( month ).getMonthlyPayment() ).getValue().compareTo( monthlyPayment.getValue() ) > 0 ) )
		? events.get( month ).getMonthlyPayment() : monthlyPayment;
      }
      interest = new Money( balance.getValueAsDouble() * interestRate );
      // check if monthly payment is greater than the money owed
      if( monthlyPayment.getValue().compareTo( interest.add( balance ).getValue() ) > 0 ) {
	monthlyPayment = interest.add( balance );
	loanReduction = monthlyPayment.subtract( interest );
	balance = balance.subtract( loanReduction );
	newTerm = month;
	bondForcastItems.add( new BondForecastItem( month, monthlyPayment, interest, loanReduction, balance ) );
	break;// breaks loop
      }
      loanReduction = monthlyPayment.subtract( interest );
      balance = balance.subtract( loanReduction );
      bondForcastItems.add( new BondForecastItem( month, monthlyPayment, interest, loanReduction, balance ) );
    }
    return bondForcastItems;
  }

  /**
   * Returns an int representing the number of months of the bond term with events.
   * 
   * @return newTerm
   */
  public int getNewTerm() {
    return newTerm;
  }

  /**
   * Calculate and return the new monthly payment needed to pay off the loan within the specified
   * term
   * 
   * @param balanceDue
   * @param interestRate
   * @param term
   * @return monthlyPayment
   */
  private Money getNewMonthlyPayment( Money balanceDue, double interestRate, int term ) {
    double ratePerAnnum = interestRate;
    double monthlyPayment = ( balanceDue.getValueAsDouble() * ratePerAnnum )
	/ ( 1 - Math.pow( ( 1 + ratePerAnnum ), - term ) );
    return new Money( monthlyPayment );
  }

  /**
   * Creates a map of BondEvents from a list of events inside a bond. The Key is the month of the
   * event start.
   * 
   * @return map BondEvents
   */
  private Map< Integer,BondEvent > mapEvents() {
    Map< Integer,BondEvent > events = new HashMap<>();
    for ( BondEvent e : bond.getBondEvents() ) {
      events.put( e.getMonth(), e );
    }
    return events;
  }
}