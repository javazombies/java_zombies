package com.javazombiez.finance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;

/**
 * Entity class that has data to be persisted.
 * 
 * @author lebo
 *
 */
@Entity
@Table
public class Investor {

  @Id
  @GeneratedValue ( strategy = GenerationType.AUTO )
  private int id;

  @Column ( unique = true )
  private long investorNumber;

  private String name;

  private String surname;

  private int identityNumber;

  private int dateOfBirth;

  private String gender;

  private int contactNumber;

  @Email
  private String email;

  private int taxNumber;

  public Investor ( ) {
  }

  public Investor ( String name, String surname, int identityNumber, int dateOfBirth, String gender, int contactNumber,
      String email, int taxNumber ) {
    super();
    this.name = name;
    this.surname = surname;
    this.identityNumber = identityNumber;
    this.dateOfBirth = dateOfBirth;
    this.gender = gender;
    this.contactNumber = contactNumber;
    this.email = email;
    this.taxNumber = taxNumber;
  }

  public int getId() {
    return id;
  }

  public void setId( int id ) {
    this.id = id;
  }

  public long getInvestorNumber() {
    return investorNumber;
  }

  public void setInvestorNumber( long investorNumber ) {
    this.investorNumber = investorNumber;
  }

  public String getName() {
    return name;
  }

  public void setName( String name ) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname( String surname ) {
    this.surname = surname;
  }

  public int getIdentityNumber() {
    return identityNumber;
  }

  public void setIdentityNumber( int identityNumber ) {
    this.identityNumber = identityNumber;
  }

  public int getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth( int dateOfBirth ) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getGender() {
    return gender;
  }

  public void setGender( String gender ) {
    this.gender = gender;
  }

  public int getContactNumber() {
    return contactNumber;
  }

  public void setContactNumber( int contactNumber ) {
    this.contactNumber = contactNumber;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail( String email ) {
    this.email = email;
  }

  public int getTaxNumber() {
    return taxNumber;
  }

  public void setTaxNumber( int taxNumber ) {
    this.taxNumber = taxNumber;
  }
}
