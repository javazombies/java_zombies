package com.javazombiez.finance.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.javazombiez.finance.utility.Money;

/**
 * Refers to any changes that might occur to the bond properties (monthly repayment, interstrate
 * etc...) during the term of the bond
 */
@Entity
public class BondEvent {

  @Id
  @GeneratedValue ( strategy = GenerationType.AUTO )
  private int id;

  private int month;

  private double interestRate;

  private Money monthlyPayment;

  public BondEvent ( int month, double interest, Money payment ) {
    super();
    this.month = month;
    this.interestRate = interest;
    this.monthlyPayment = payment;
  }

  public BondEvent ( ) {
  }

  public int getMonth() {
    return month;
  }

  public void setMonth( int month ) {
    this.month = month;
  }

  public double getInterestRate() {
    return interestRate;
  }

  public void setInterestRate( double interest ) {
    this.interestRate = interest;
  }

  public Money getMonthlyPayment() {
    return monthlyPayment;
  }

  public void setPayment( Money payment ) {
    this.monthlyPayment = payment;
  }

  @Override
  public String toString() {
    return "BondEvent [month=" + month + ", interestRate=" + interestRate + ", monthlyPayment=" + monthlyPayment + "]";
  }
  
}
