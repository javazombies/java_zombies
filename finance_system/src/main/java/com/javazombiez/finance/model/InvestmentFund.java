package com.javazombiez.finance.model;

/**
 * 
 * @author Ndumiso
 *
 */
public enum InvestmentFund {
  TAX_FREE ( "Tax Free Investment"), MONEY_MARKET ( "Money Market"), EQUITY_FUND ( "Equity Fund");

  private String name;

  InvestmentFund ( String name ) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
