package com.javazombiez.finance.model;

import com.javazombiez.finance.utility.Money;

/**
 * A class that models forcast items such as opening balance and closing balance for a specific month
 * @author Teddy
 *
 */
public class ForecastItem {
   private int month;
   private Money openingBalance;
   private Money contribution;
   private 
   double interestRate;
   
  public double getInterestRate() {
    return interestRate;
  }

  
  public void setInterestRate( double interestRate ) {
    this.interestRate = interestRate;
  }

  private Money interestEarned;
   private Money closingBalance;
   
  

  public ForecastItem ( int month, Money openingBalance, Money contribution, double interestRate, Money interestEarned,
      Money closingBalance ) {
    super();
    this.month = month;
    this.openingBalance = openingBalance;
    this.contribution = contribution;
    this.interestRate = interestRate;
    this.interestEarned = interestEarned;
    this.closingBalance = closingBalance;
  }


  public int getMonth() {
    return month;
  }
  
  public void setMonth( int month ) {
    this.month = month;
  }
  
  public Money getOpeningBalance() {
    return openingBalance;
  }
  
  public void setOpeningBalance( Money openingBalance ) {
    this.openingBalance = openingBalance;
  }
  
  public Money getContribution() {
    return contribution;
  }
  
  public void setContribution( Money contribution ) {
    this.contribution = contribution;
  }
  
  public Money getInterestEarned() {
    return interestEarned;
  }
  
  public void setInterestEarned( Money interestEarned ) {
    this.interestEarned = interestEarned;
  }
  
  public Money getClosingBalance() {
    return closingBalance;
  }
  
  public void setClosingBalance( Money closingBalance ) {
    this.closingBalance = closingBalance;
  }
   
}
