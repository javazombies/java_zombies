package com.javazombiez.finance.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.javazombiez.finance.utility.Money;

@Entity
@Table
public class Investment implements BusinessProduct {

  @Id
  @GeneratedValue ( strategy = GenerationType.IDENTITY )
  private int id;

  /**
   * Regular monthly deposits.
   */
  @Embedded
  @Column ( nullable = false )
  @AttributeOverrides ( {@AttributeOverride ( name = "value", column = @Column ( name = "contribution" ) )} )
  private Money contribution;

  /**
   * The interest rate
   */
  @Column ( nullable = false )
  private double interestRate;

  /**
   * The term of the investment in months.
   */
  @Column ( nullable = false )
  private int term;

  @ManyToOne
  @JoinColumn ( name = "investor_Id", nullable = false, referencedColumnName = "id" )
  private Investor investor;

  @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
  @Column ( nullable = false )
  @JoinColumn ( name = "eventId", referencedColumnName = "id" )
  private List< Event > events = new ArrayList< Event >();

	private InvestmentFund investmentFund;

  
	public void setInvestmentFund(InvestmentFund investmentFund) {
		this.investmentFund = investmentFund;
	}

	public Investment ( ) {
  }

  public Investment ( double contribution, int term, double interestRate ) {
    this.contribution = new Money( contribution );
    this.term = term;
    this.interestRate = interestRate;
  }

  public int getId() {
    return id;
  }

  public void setId( int id ) {
    this.id = id;
  }

  public Money getContribution() {
    return contribution;
  }

  public void setContribution( Money contribution ) {
    this.contribution = contribution;
  }

  public double getInterestRate() {
    return interestRate;
  }

  public void setInterestRate( double interestRate ) {
    this.interestRate = interestRate;
  }

  public int getTerm() {
    return term;
  }

  public void setTerm( int term ) {
    this.term = term;
  }

  public Investor getInvestor() {
    return investor;
  }

  public void setInvestor( Investor investor ) {
    this.investor = investor;
  }

  public List< Event > getEvents() {
    return events;
  }

  public void setEvents( List< Event > events ) {
    this.events = events;
  }

  public void addEvent( Event event ) {
    events.add( event );
  }

	public InvestmentFund getInvestmentFund() {
		// TODO Auto-generated method stub
		return investmentFund;
	}
}
