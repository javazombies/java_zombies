package com.javazombiez.finance.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.javazombiez.finance.utility.Money;
import com.javazombiez.finance.utility.TaxUtil;

/**
 * An abstraction of a Loan, Mortgage, property bond, car finance etc...
 */
@Entity
@Table
public class PropertyBond implements BusinessProduct{

  private static final double LEGAL_FEE_RATE = 0.008;

  static public final Money REGISTRATION_FEE = new Money( 5_000 );

  static public final Money MINIMUM_LEGAL_FEE = new Money( 15_000 );

  /**
   * The amount to be borrowed.
   */
  @Id
  @GeneratedValue ( strategy = GenerationType.IDENTITY )
  private int id;

  @Embedded
  @Column ( nullable = false )
  @AttributeOverrides ( {@AttributeOverride ( name = "value", column = @Column ( name = "principal_amount" ) )} )
  private Money principalBondAmount;

  /**
   * rate per annum.
   */
  private double interestRate;

  @Embedded
  @Column ( nullable = false )
  @AttributeOverrides ( {@AttributeOverride ( name = "value", column = @Column ( name = "deposit" ) )} )
  private Money deposit;

  /**
   * The number of months over which the loan will be repayed.
   */
  private int term;

  /**
   * The total amount currently owed to the borrower.
   */
  @Embedded
  @Column ( nullable = false )
  @AttributeOverrides ( {@AttributeOverride ( name = "value", column = @Column ( name = "balance_due" ) )} )
  private Money balanceDue;

  @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
  @Column ( nullable = false )
  @JoinColumn ( name = "eventId", referencedColumnName = "id" )
  private List< BondEvent > BondEvents = new ArrayList<>();

  @ManyToOne
  @JoinColumn ( name = "investor_id", referencedColumnName = "id" )
  private Investor investorNumber;

  public PropertyBond ( Money principalBondAmount, double interestRate, Money deposit, int term ) {
    super();
    this.principalBondAmount = principalBondAmount;
    this.interestRate = interestRate;
    this.deposit = deposit;
    this.term = term;
    subtractDeposit();
    setBalanceDue( principalBondAmount );
    // addBondCostsToBalance(); // add extra costs.
  }

  public PropertyBond ( ) {
  }

  // =========getters and setters===========
  public int getId() {
    return id;
  }

  public void setId( int id ) {
    this.id = id;
  }

  public Money getPrincipalAmount() {
    return principalBondAmount;
  }

  public void setPrincipalBondAmount( Money principalBondAmount ) {
    this.principalBondAmount = principalBondAmount;
  }

  public double getInterestRate() {
    return interestRate;
  }

  public void setInterestRate( double interestRate ) {
    this.interestRate = interestRate;
  }

  public Money getDeposit() {
    return deposit;
  }

  public void setDeposit( Money deposit ) {
    this.deposit = deposit;
  }

  public int getTerm() {
    return term;
  }

  public void setTerm( int term ) {
    this.term = term;
  }

  public Money getBalanceDue() {
    return balanceDue;
  }

  public void setDueBalance( Money BalanceDue ) {
    this.balanceDue = BalanceDue;
  }

  public List< BondEvent > getBondEvents() {
    return BondEvents;
  }

  
	public Investor getInvestorNumber() {
		return investorNumber;
	}

	
	public void setInvestorNumber(Investor investorNumber) {
		this.investorNumber = investorNumber;
	}

	public void setBondEvents( List< BondEvent > bondEvents ) {
    BondEvents = bondEvents;
  }

  public Money getPrincipalBondAmount() {
    return principalBondAmount;
  }

  public void setBalanceDue( Money balanceDue ) {
    this.balanceDue = balanceDue;
  }

  // ================ Logic =====================
  /**
   * Calculates and returns the minimum monthly repayment required over the term of the bond.;
   */
  public Money getMonthlyPayment() {
    double ratePerAnnum = ( ( this.getInterestRate() / 100 ) / 12 );
    double monthlyPayment = ( this.getPrincipalAmount().getValueAsDouble() * ratePerAnnum )
	/ ( 1 - Math.pow( ( 1 + ratePerAnnum ), - this.getTerm() ) );
    return new Money( monthlyPayment );
  }

  private void subtractDeposit() {
    this.setPrincipalBondAmount(
	new Money( this.getPrincipalAmount().subtract( this.getDeposit() ).getValueAsDouble() ) );
  }

  // /**
  // * adds bond costs to the principal amount.
  // */
  // private void addBondCostsToBalance() {
  // // legal fees
  // this.setBalanceDue(this.getBalanceDue().add(this.getLegalFees()));
  // // registration fee
  // this.setBalanceDue(this.getBalanceDue().add(REGISTRATION_FEE));
  // // transfer costs
  // Money transferDuty =
  // TaxUtil.calculateTransferDuty(this.getPrincipalAmount().getValueAsDouble());
  // this.setBalanceDue(getBalanceDue().add(transferDuty)); // random
  // }
  public Money getLegalFees() {
    Money legalFee = new Money( LEGAL_FEE_RATE * this.getPrincipalAmount().getValueAsDouble() );
    if( legalFee.getValueAsDouble() > MINIMUM_LEGAL_FEE.getValueAsDouble() )
      return legalFee;
    else
      return MINIMUM_LEGAL_FEE;
  }

  /**
   * Returns the total amount repayable at the end of the term including interest and costs.
   */
  public Money getTotalPayable() {
    return this.getBalanceDue().add( this.getTotalInterest() );
  }

  /**
   * Returns the total interest accumulated during the term of the bond.
   *
   */
  public Money getTotalInterest() {
    double totalInterestPaid = this.getMonthlyPayment().getValueAsDouble() * this.getTerm();
    return new Money( totalInterestPaid ).subtract( this.getPrincipalAmount() );
  }

  public Money getTransferDuty() {
    return TaxUtil.calculateTransferDuty( this.getPrincipalAmount().getValueAsDouble() );
  }
}