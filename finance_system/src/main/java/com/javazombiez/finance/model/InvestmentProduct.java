package com.javazombiez.finance.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Encapsulates a type of investent product.
 * 
 * @author Ndumiso.
 *
 */
@Entity
@Table
public class InvestmentProduct {

  @Id
  @GeneratedValue ( strategy = GenerationType.IDENTITY )
  private int id;

  @OneToOne
  @JoinColumn ( referencedColumnName = "id" )
  Investment investment;

  private String fund;

  public InvestmentProduct ( InvestmentFund fund ) {
    this.fund = fund.getName();
  }

  public InvestmentProduct ( ) {
  }

  public Investment getInvestment() {
    return investment;
  }

  public void setInvestment( Investment investment ) {
    this.investment = investment;
  }

  public String getName() {
    return fund;
  }
}
