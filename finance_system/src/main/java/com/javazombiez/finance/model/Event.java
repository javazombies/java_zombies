package com.javazombiez.finance.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.javazombiez.finance.utility.Money;

@Entity
public class Event {

  @Id
  @GeneratedValue ( strategy = GenerationType.IDENTITY )
  private int id;

  private int month;

  @Embedded
  @Column ( nullable = false )
  @AttributeOverrides ( value = {@AttributeOverride ( column = @Column ( name = "contribution" ), name = "value" )} )
  private Money contribution;

  private double interestRate;

  public Event ( ) {
  }

  public Event ( int month, Money contribution, double interestRate ) {
    super();
    this.month = month;
    this.contribution = contribution;
    this.interestRate = interestRate;
  }

  public int getId() {
    return id;
  }

  public void setId( int id ) {
    this.id = id;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth( int month ) {
    this.month = month;
  }

  public Money getContribution() {
    return contribution;
  }

  public void setContribution( Money contribution ) {
    this.contribution = contribution;
  }

  public double getInterestRate() {
    return interestRate;
  }

  public void setInterestRate( double interestRate ) {
    this.interestRate = interestRate;
  }
}