package com.javazombiez.finance.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javazombiez.finance.model.BondEvent;
import com.javazombiez.finance.model.Event;
import com.javazombiez.finance.model.Forecast;
import com.javazombiez.finance.model.Investment;
import com.javazombiez.finance.model.InvestmentFund;
import com.javazombiez.finance.model.InvestmentProduct;
import com.javazombiez.finance.model.PropertyBond;
import com.javazombiez.finance.service.InvestmentService;
import com.javazombiez.finance.utility.Money;

/**
 * An investment controller handling and processing all requests related to an
 * investment.
 * 
 * @author Null
 *
 */
@RequestScoped
@Named(value = "investmentController")
public class InvestmentController implements Controller {

  // Investment investment = null;

  @Inject
  private InvestmentService investmentService;

  public void calculateFutureValueAtFixedRate(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    Investment investment = buildInvestmentObject(request, response);
    List<Event> events = buildInvestmentEventList(request, response);
    investment.setEvents(events);
    setInvestmentEventsAttributes(request);
    setInvestmentParameters(request, investment);
    setFormAttrbutes(request);
    request.setAttribute("forecast", investmentService.calculateFutureValueAtFixedRate(investment));
    request.setAttribute("investment", investment);
    String action = request.getParameter("action");
    if (action.equals("Calculate"))
      request.getRequestDispatcher("/WEB-INF/investmentCalculator.jsp").forward(request, response);
    else {
      request.getRequestDispatcher("/WEB-INF/investmentclient.jsp").forward(request, response);
    }
  }

  private Investment buildInvestmentObject(HttpServletRequest request, HttpServletResponse response) {
    double contribution = Double.parseDouble((request.getParameter("monthlyContribution")));
    int term = Integer.parseInt(request.getParameter("investmentTerm"));
    double interestRate = Double.parseDouble(request.getParameter("rate"));
    String fund = request.getParameter("investmentFund");
    Investment investment = new Investment(contribution, term, interestRate);
    if (fund.endsWith("Fund"))
      investment.setInvestmentFund(InvestmentFund.EQUITY_FUND);
    else if (fund.endsWith("Market"))
      investment.setInvestmentFund(InvestmentFund.MONEY_MARKET);
    else
      investment.setInvestmentFund(InvestmentFund.TAX_FREE);

    return investment;
  }

  private void setInvestmentParameters(HttpServletRequest request, Investment investment) {
    request.setAttribute("param_monthlyContribution",
	new Money(Double.parseDouble(request.getParameter("monthlyContribution"))));
    request.setAttribute("param_investmentTerm", Integer.parseInt(request.getParameter("investmentTerm")));
    request.setAttribute("param_rate", Double.parseDouble(request.getParameter("rate")));
    // request.setAttribute("param_investmentFund",request.getParameter("investmentFund"));
  }

  public List<Event> buildInvestmentEventList(HttpServletRequest request, HttpServletResponse response) {
    List<Event> events = new ArrayList<>();

    for (int i = 1; i <= 5; i++) {

      Money monthlyContribution = (request.getParameter("eventContribution" + i) != null
	  && !request.getParameter("eventContribution" + i).equals(""))
	      ? new Money(Double.parseDouble(request.getParameter("eventContribution" + i))) : null;
      int investmentTerm = (request.getParameter("eventTerm" + i) != null
	  && !request.getParameter("eventTerm" + i).equals(""))
	      ? Integer.parseInt(request.getParameter("eventTerm" + i)) : 0;
      double rate = (request.getParameter("eventRate" + i) != null && !request.getParameter("eventRate" + i).equals(""))
	  ? Double.parseDouble(request.getParameter("eventRate" + i)) : 0;

      if (!(investmentTerm == 0 || (rate == 0 && monthlyContribution == null)))
	events.add(new Event(investmentTerm, monthlyContribution, rate));
    }
    for (Event investmentEvent : events) {
      System.out.println(investmentEvent);
    }
    return events;
  }

  private void setInvestmentEventsAttributes(HttpServletRequest request) {
    for (int i = 1; i <= 5; i++) {
      request.setAttribute("eventContribution" + i, request.getParameter("eventContribution" + i));
      request.setAttribute("eventTerm" + i, request.getParameter("eventTerm" + i));
      request.setAttribute("eventRate" + i, request.getParameter("eventRate" + i));
    }
  }

  private void setFormAttrbutes(HttpServletRequest request) {
    request.setAttribute("monthlyContribution", request.getParameter("monthlyContribution"));
    request.setAttribute("investmentTerm", request.getParameter("investmentTerm"));
    request.setAttribute("rate", request.getParameter("rate"));
  }

}
