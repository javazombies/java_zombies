package com.javazombiez.finance.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javazombiez.finance.model.BondEvent;
import com.javazombiez.finance.model.BusinessProduct;
import com.javazombiez.finance.model.Event;
import com.javazombiez.finance.model.Investment;
import com.javazombiez.finance.model.Investor;
import com.javazombiez.finance.model.PropertyBond;
import com.javazombiez.finance.service.BondService;
import com.javazombiez.finance.service.InvestmentService;
import com.javazombiez.finance.service.InvestorService;

/**
 * An investor controller handling and processing all requests related to an
 * investor.
 * 
 * @author Teddy, Teboho
 *
 */
@Named(value = "investorController")
public class InvestorController implements Controller {

	/**
	 * Adds an investor
	 *
	 */
	@Inject
	private InvestorService investorService;

	@Inject
	private InvestmentService investmentService;

	@Inject
	private BondService bondService;

	/**
	 * Creates a new client and persist it to the database
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Investor investor = getObjectFromParameters(request);
		Investment investment = ((Investment) request.getSession().getAttribute("investmentImpl"));
		investment.setInvestor(investor);
		investmentService.createInvestment(investor, investment);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/success.jsp");
		dispatcher.forward(request, response);
	}

	public void addInvestmentToExistingClient(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int idNumber = Integer.parseInt(request.getParameter("identity_number"));
		Investment investment = ((Investment) request.getSession().getAttribute("investmentImpl"));
		investmentService.addBondToExistingCustomer(idNumber, investment);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/success.jsp");
		dispatcher.forward(request, response);
	}

	public void addNewClientWithBond(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Investor investor = getObjectFromParameters(request);
		PropertyBond propertyBond = (PropertyBond) request.getSession().getAttribute("bondImpl");
		propertyBond.setInvestorNumber(investor);
		bondService.addBond(investor, propertyBond);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/success.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * Adds the new selected bond product to existing client.
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException
	 */
	public void addBondToExistingClient(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int idNumber = Integer.parseInt(request.getParameter("identity_number"));
		PropertyBond bond = (PropertyBond) request.getSession().getAttribute("bondImpl");
		bondService.addBondToExistingCustomer(idNumber, bond);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/success.jsp");
		dispatcher.forward(request, response);
	}

	private Investor getObjectFromParameters(HttpServletRequest request) {
		String name = request.getParameter("first_name");
		String surname = request.getParameter("surname");
		String gender = request.getParameter("gender");
		int identityNumber = Integer.parseInt(request.getParameter("identity_number"));
		int dateOfBirth = convertRequestStringDateToInt(request.getParameter("date_of_birth"));
		int contactNumber = Integer.parseInt(request.getParameter("contact_number"));
		String email = request.getParameter("email");
		int taxNumber = Integer.parseInt(request.getParameter("tax_number"));
		Investor investor = new Investor(name, surname, identityNumber, dateOfBirth, gender, contactNumber, email,
				taxNumber);
		return investor;
	}

	private int convertRequestStringDateToInt(String date) {
		String[] date1 = date.split("-");
		return Integer.parseInt(date1[0]) * 10000 + Integer.parseInt(date1[1]) * 100 + Integer.parseInt(date1[2]);
	}

	public void getAllInvestors(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Investor> investors = investorService.getAllInvestors();
		request.getSession().setAttribute("investors", investors);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/viewclients.jsp");
		dispatcher.forward(request, response);
	}

	public void getInvestorProducts(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		long investorNumber = Long.parseLong(request.getParameter("investornumber"));

		List<BusinessProduct> products = investorService.getinvestorProduts(investorNumber);
		List<PropertyBond> bondProducts = new ArrayList<PropertyBond>();
		List<Investment> investmentProducts = new ArrayList<Investment>();

		for (BusinessProduct product : products) {
			if (product instanceof PropertyBond) {
				bondProducts.add((PropertyBond) product);
			}
			else {
				investmentProducts.add((Investment) product);
			}
		}

		request.getSession().setAttribute("bondProducts", bondProducts);
		request.getSession().setAttribute("investmentProducts", investmentProducts);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/viewclients.jsp");
		dispatcher.forward(request, response);
	}

	public void getBondEvents(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int bondId = Integer.parseInt(request.getParameter("bondid"));
		List<BondEvent> bondEvents = (List<BondEvent>) bondService.getEvents(bondId);
		request.setAttribute("bondEvents", bondEvents);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/viewclients.jsp");
		dispatcher.forward(request, response);
	}

	public void getInvestmentEvents(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int investmentId = Integer.parseInt(request.getParameter("investmentid"));
		List<Event> investmentEvents = (List<Event>) investmentService.getEvents(investmentId);
		request.setAttribute("investmentEvents", investmentEvents);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/viewclients.jsp");
		dispatcher.forward(request, response);
	}
}
