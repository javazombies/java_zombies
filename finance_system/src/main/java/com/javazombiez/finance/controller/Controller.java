package com.javazombiez.finance.controller;

/**
 * Marker Interface for all controllers.
 * 
 * @author Java Zombies
 *
 */
public interface Controller {
}
