
package com.javazombiez.finance.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javazombiez.finance.model.BondEvent;
import com.javazombiez.finance.model.PropertyBond;
import com.javazombiez.finance.service.BondService;
import com.javazombiez.finance.utility.Money;

@RequestScoped
@Named ( "bondController" )
public class BondController implements Controller {

  @Inject
  private BondService bondService;

  public void calculateMonthlyRepayment( HttpServletRequest request, HttpServletResponse response )
      throws IOException, ServletException {
    PropertyBond bond = buildBondObject( request, response );
    List< BondEvent > bondEvents = buildBondEventList( request, response );
    bond.setBondEvents( bondEvents );
    request.setAttribute( "bond", bond );
    setBondEventsAttributes( request );
    setPropertyBondParameters( request, bond );
    request.setAttribute( "bondForecastItems", bondService.getBondForecastItems( bond ) );
    String action = request.getParameter( "action" );
    if(action.equals( "calculate" ) )
      request.getRequestDispatcher( "/WEB-INF/bondCalculator.jsp" ).forward( request, response );
    else{
      request.getRequestDispatcher( "/WEB-INF/client.jsp" ).forward( request, response );;
    }
  }

  private void setPropertyBondParameters( HttpServletRequest request, PropertyBond bond ) {
    request.setAttribute( "propertyBond", bond );
    request.setAttribute( "rate", bond.getInterestRate() );
    request.setAttribute( "term", bond.getTerm());
    request.setAttribute( "deposit", bond.getDeposit() );
    request.setAttribute( "depositInput", bond.getDeposit().getValueAsDouble() );
    request.setAttribute( "monthlyPayment", bond.getMonthlyPayment() );
    request.setAttribute( "totalPayment", bond.getTotalPayable() );
    request.setAttribute( "totalInterest", bond.getTotalInterest() );
    request.setAttribute( "purchaseAmount", bond.getPrincipalAmount() );
    request.setAttribute( "loan", request.getParameter( "loan" ) );
    request.setAttribute( "registrationFee", PropertyBond.REGISTRATION_FEE );
    request.setAttribute( "legalFees", bond.getLegalFees() );
    request.setAttribute( "tranferCosts", bond.getTransferDuty() );
    request.setAttribute( "param_interest", new Money( Double.parseDouble( request.getParameter( "rate" ) ) ) );
    request.setAttribute( "param_deposit", new Money( Double.parseDouble( request.getParameter( "deposit" ) ) ) );
  }

  private PropertyBond buildBondObject( HttpServletRequest request, HttpServletResponse response ) {
    double principalAmount = Double.parseDouble( ( request.getParameter( "loan" ) ) );
    double interestRate = Double.parseDouble( request.getParameter( "rate" ) );
    double deposit = Double.parseDouble( request.getParameter( "deposit" ) );
    int term = Integer.parseInt( request.getParameter( "term" ) );
    return new PropertyBond( new Money( principalAmount ), interestRate, new Money( deposit ), term );
  }

  private List< BondEvent > buildBondEventList( HttpServletRequest request, HttpServletResponse response ) {
    List< BondEvent > bondEvents = new ArrayList<>();
    for ( int i = 1 ; i <= 5 ; i++ ) {
      int month = ( request.getParameter( "eventMonth" + i ) != null
	  && ! request.getParameter( "eventMonth" + i ).equals( "" ) )
	      ? Integer.parseInt( request.getParameter( "eventMonth" + i ) ) : 0;
      double interestRate = ( request.getParameter( "eventInterest" + i ) != null
	  && ! request.getParameter( "eventInterest" + i ).equals( "" ) )
	      ? Double.parseDouble( request.getParameter( "eventInterest" + i ) ) : 0;
      Money monthlyPayment = ( request.getParameter( "eventPayment" + i ) != null
	  && ! request.getParameter( "eventPayment" + i ).equals( "" ) )
	      ? new Money( Double.parseDouble( request.getParameter( "eventPayment" + i ) ) ) : null;
      if( ! ( month == 0 || ( interestRate == 0 && monthlyPayment == null ) ) )
	bondEvents.add( new BondEvent( month, interestRate, monthlyPayment ) );
    }
    for ( BondEvent event : bondEvents ) {
      System.out.println( event );
    }
    return bondEvents;
  }

  private void setBondEventsAttributes( HttpServletRequest request ) {
    for ( int i = 1 ; i <= 5 ; i++ ) {
      request.setAttribute( "eventMonth" + i, request.getParameter( "eventMonth" + i ) );
      request.setAttribute( "eventInterest" + i, request.getParameter( "eventInterest" + i ) );
      request.setAttribute( "eventPayment" + i, request.getParameter( "eventPayment" + i ) );
    }
  }
}
