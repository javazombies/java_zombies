package com.javazombiez.finance.application;

import com.javazombiez.finance.model.PropertyBond;
import com.javazombiez.finance.utility.Money;

public class ApplicationBond {

  public static void main( String[] args ) {
    Money principalBondAmount = new Money( 1_000_000 );
    double interestRate = 10.5;
    Money deposit = new Money();
    int term = 240;
    PropertyBond bond = new PropertyBond( principalBondAmount, interestRate, deposit, term );
    System.out.println( "Monthly repayment\t Total Payable\t Total Interest\n" );
    System.out.println( bond.getMonthlyPayment() + "\t\t " + bond.getTotalPayable() + "\t " + bond.getTotalInterest() );
  }
}