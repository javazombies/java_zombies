package com.javazombiez.finance.application;

import com.javazombiez.finance.model.Investor;
import com.javazombiez.finance.model.PropertyBond;
import com.javazombiez.finance.service.InvestorServiceImpl;

public class ApplicationRetrieve {

	public static void main(String[] args) {
		InvestorServiceImpl investorServiceImpl = new InvestorServiceImpl();

		Investor investor = investorServiceImpl.getAllInvestors().get(0);
		PropertyBond bond = new PropertyBond();

		System.out.println("The name is " + investor.getName() + " and the bond is ");
	}
}
