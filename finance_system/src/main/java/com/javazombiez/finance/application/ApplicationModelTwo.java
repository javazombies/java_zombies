package com.javazombiez.finance.application;

import java.util.Scanner;

import com.javazombiez.finance.model.Event;
import com.javazombiez.finance.model.Forecast;
import com.javazombiez.finance.model.ForecastItem;
import com.javazombiez.finance.model.Investment;
import com.javazombiez.finance.service.InvestmentService;
import com.javazombiez.finance.service.InvestmentServiceImpl;
import com.javazombiez.finance.utility.Money;

public class ApplicationModelTwo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		InvestmentService investmentService = new InvestmentServiceImpl();
		Investment investment = new Investment();
		Forecast forecast = null;

		double monthlyContribution = 0;
		double interestRate = 0;
		int term = 0;

		int j = 2;
		for (; j != 3;) {
			int i = 0;
			int eventCounter = 0;
			for (; i != 1;) {
				System.out.println("enter monthlyContrbution, interestRate, and term");
				monthlyContribution = input.nextDouble();
				interestRate = input.nextDouble();
				term = input.nextInt();
				if (eventCounter <= 0) {
					investment.setContribution(new Money(monthlyContribution));
					investment.setInterestRate(interestRate);
					investment.setTerm(term);
				}
				else {
					Event event = new Event(term, (monthlyContribution==0)? null :new Money(monthlyContribution), interestRate);
					investment.addEvent(event);
				}
				eventCounter++;
				System.out.println("press 0 to enter more event, OR 1 to calculate forecust");
				i = input.nextInt();
			}
			forecast = investmentService.calculateFutureValueAtFixedRate(investment);

			printForcast(forecast);
			System.out.println("press 2 to forecast another investment, OR 3 to exit");
			j = input.nextInt();
		}

		System.out.println("==============================================");
		System.out.println("Thank you fo using our system");
		System.out.println("==============================================");
	}
	private static void printForcast(Forecast forcast){
		System.out.println("MONTH\tOPENING BALANCE\tCONTRIBUTION\tINTEREST\tINTEREST EARNED\tCLOSING BALANCE");
		for(ForecastItem item : forcast.getForcastItems()){
			System.out.println(item.getMonth()+"\t"+item.getOpeningBalance()+"\t"+item.getContribution()+"\t"+item.getInterestRate()+"\t"+item.getInterestEarned()+"\t"+item.getClosingBalance());
		}
	}
}
