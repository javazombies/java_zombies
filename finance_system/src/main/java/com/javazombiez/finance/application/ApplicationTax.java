package com.javazombiez.finance.application;

import java.util.Scanner;

import com.javazombiez.finance.utility.Money;
import com.javazombiez.finance.utility.TaxUtil;

public class ApplicationTax {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Please enter your bond amount");
		double bondAmount = input.nextDouble();

		Money transferDuty = TaxUtil.calculateTransferDuty(bondAmount);
		System.out.println("Your Nett is : R " + transferDuty);
	}
}
