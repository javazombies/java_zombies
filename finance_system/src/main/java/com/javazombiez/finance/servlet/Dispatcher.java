package com.javazombiez.finance.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javazombiez.finance.controller.Controller;

/**
 * The dispatcher @Controller
 * 
 * @author Ndumiso.
 *
 */
@SuppressWarnings ( "serial" )
@WebServlet ( urlPatterns = {"/investor/*", "/investment/*", "/bond/*"} )
public class Dispatcher extends HttpServlet {

  @Inject
  @Any
  private Instance< Controller > controllers;

  private final static Properties PROPERTIES = new Properties();

  @Override
  protected void doGet( HttpServletRequest request, HttpServletResponse response )
      throws ServletException, IOException {
    RequestDispatcher dispatcher = request.getRequestDispatcher( "/WEB-INF" + request.getPathInfo() + ".jsp" );
    if( dispatcher != null )
      dispatcher.forward( request, response );
  }

  @Override
  protected void doPost( HttpServletRequest request, HttpServletResponse response )
      throws ServletException, IOException {
    doHandleRequest( request, response );
  }

  private void doHandleRequest( HttpServletRequest request, HttpServletResponse response ) throws IOException {
    try {
      String[] split = PROPERTIES.getProperty( request.getPathInfo() ).split( "#" );
      BeanManager beanManager = CDI.current().getBeanManager();
      Bean bean = beanManager.getBeans( split[0] ).iterator().next();
      CreationalContext context = beanManager.createCreationalContext( bean );
      Controller requestController = (Controller) beanManager.getReference( bean, bean.getBeanClass(), context );
      requestController.getClass().getMethod( split[1], HttpServletRequest.class, HttpServletResponse.class )
	  .invoke( requestController, request, response );
    }
    catch ( Exception e ) {
      throw new RuntimeException( e );
    }
  }

  @Override
  public void init() throws ServletException {
    try {
      InputStream inputStream = Dispatcher.class.getClassLoader().getResourceAsStream( "controller.properties" );
      PROPERTIES.load( inputStream );
    }
    catch ( IOException e ) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }
}
