package com.javazombiez.finance.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import com.javazombiez.finance.model.Event;
import com.javazombiez.finance.model.Forecast;
import com.javazombiez.finance.model.Investment;
import com.javazombiez.finance.model.Investor;
import com.javazombiez.finance.resource.InvestmentEventResource;
import com.javazombiez.finance.resource.InvestmentResource;
import com.javazombiez.finance.resource.InvestorResource;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class InvestmentServiceImpl implements InvestmentService {

	@Inject
	private InvestmentResource investmentResource;
	
	@Inject
	private InvestmentEventResource investmentEventResource;
	
	@Inject
	private InvestorResource investorResource;

	@Override
	public Forecast calculateFutureValueAtFixedRate(Investment investment) {
		return new Forecast( investment );
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void createInvestment(Investor investor, Investment investment) {
		investorResource.addInvestor(investor);
		investmentResource.createInvestment(investment);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addBondToExistingCustomer(int idNumber, Investment investment) {
		Investor investor = investorResource.getInvestor(idNumber);
		investment.setInvestor(investor);
		investmentResource.createInvestment(investment);
	}

	@Override
	public List<Event> getEvents(int investmentId) {
		return investmentEventResource.getEvents(investmentId);
	}

}
