package com.javazombiez.finance.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.javazombiez.finance.model.BusinessProduct;
import com.javazombiez.finance.model.Investor;
import com.javazombiez.finance.model.PropertyBond;
import com.javazombiez.finance.resource.InvestorResource;

/**
 * @author lebo, Teboho
 */
@Stateless
public class InvestorServiceImpl implements InvestorService {

	@Inject
	private InvestorResource investorResource;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void add(Investor investor) {
		investorResource.addInvestor(investor);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Investor> getAllInvestors() {
		return investorResource.getListOfInvestors();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<BusinessProduct> getinvestorProduts(long investorNumber) {
		return investorResource.getInvestorProducts(investorNumber);
	}
}
