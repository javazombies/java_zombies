package com.javazombiez.finance.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.javazombiez.finance.model.BondEvent;
import com.javazombiez.finance.model.BondForecast;
import com.javazombiez.finance.model.BondForecastItem;
import com.javazombiez.finance.model.Investor;
import com.javazombiez.finance.model.PropertyBond;
import com.javazombiez.finance.resource.BondEventResource;
import com.javazombiez.finance.resource.BondResource;
import com.javazombiez.finance.resource.InvestorResource;

/**
 * 
 * @author Teboho
 *
 */
@Stateless
public class BondServiceImpl implements BondService {

	@Inject
	private BondResource bondResource;
	@Inject
	private BondEventResource bondEventResource;

	@Inject
	private InvestorResource investorResource;

	public List<BondForecastItem> getBondForecastItems(PropertyBond bond) {
		return new BondForecast(bond).getBondForcastItems();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addBond(Investor investor, PropertyBond bond) {
		investorResource.addInvestor(investor);
		bondResource.addBond(bond);
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addBondToExistingCustomer(int idNumber, PropertyBond bond) {
		Investor investor = investorResource.getInvestor(idNumber);
		bond.setInvestorNumber(investor);
		bondResource.addBond(bond);
	}

	@Override
	public List<BondEvent> getEvents(int bondId) {
		return bondEventResource.getEvents(bondId);
	}

}
