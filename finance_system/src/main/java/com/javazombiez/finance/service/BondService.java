package com.javazombiez.finance.service;

import java.util.List;

import javax.ejb.Local;

import com.javazombiez.finance.model.BondEvent;
import com.javazombiez.finance.model.BondForecastItem;
import com.javazombiez.finance.model.Investor;
import com.javazombiez.finance.model.PropertyBond;

@Local
public interface BondService {

	public List<BondForecastItem> getBondForecastItems(PropertyBond bond);
	
	public abstract void addBond(Investor investor,PropertyBond bond);
	
	public abstract void addBondToExistingCustomer(int idNumber, PropertyBond bond);
	
	public abstract List<BondEvent> getEvents(int bondId);
}
