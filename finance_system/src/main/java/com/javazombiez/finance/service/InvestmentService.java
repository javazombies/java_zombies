package com.javazombiez.finance.service;

import java.util.List;

import javax.ejb.Local;

import com.javazombiez.finance.model.Event;
import com.javazombiez.finance.model.Forecast;
import com.javazombiez.finance.model.Investment;
import com.javazombiez.finance.model.Investor;


/**
 * Investment service responsible for an investment's business logic.
 * 
 * @author Java Zombiez
 *
 */
@Local
public interface InvestmentService {

  /**
   * Returns a forecast object hat shows how the investment grows per month
   * 
   * @return Forecast 
   */
   Forecast calculateFutureValueAtFixedRate( Investment investment );

  /**
   * Inserts an investment record into the investment table.
   */
  public abstract void createInvestment( Investor investor,Investment investment );
  
  public abstract void addBondToExistingCustomer(int idNumber,Investment investment);
  
  public abstract List<Event> getEvents(int investmentId);
  
}