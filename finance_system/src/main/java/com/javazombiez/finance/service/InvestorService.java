package com.javazombiez.finance.service;

import java.util.List;

import javax.ejb.Local;

import com.javazombiez.finance.model.BusinessProduct;
import com.javazombiez.finance.model.Investor;
import com.javazombiez.finance.model.PropertyBond;

/**
 * 
 * @author Teboho
 *
 */
@Local
public interface InvestorService {

	public abstract void add(Investor investor);

	public abstract List<Investor> getAllInvestors();
	
	public abstract List<BusinessProduct> getinvestorProduts(long investorNumber);

}
