package com.javazombiez.finance.test;

import org.junit.Test;

import com.javazombiez.finance.model.PropertyBond;
import com.javazombiez.finance.utility.Money;

import junit.framework.TestCase;

public class testBondService extends TestCase {

  @SuppressWarnings ( "unused" )
  private static final PropertyBond PROPERTY_BOND = new PropertyBond( new Money( 1_000_000 ), 12, new Money( 30_000 ), 240 );

  @Test
  public void testMonthlyPayment() {
    assertEquals( new Money( 10_680.54 ), PROPERTY_BOND.getMonthlyPayment() );
  }

  @Test
  public void testTotalPayment() {
    assertEquals( new Money( 2_514_050.40 ), PROPERTY_BOND.getTotalPayable() );
  }
}
