package com.javazombiez.finance.test;

import org.junit.Test;

import com.javazombiez.finance.model.Forecast;
import com.javazombiez.finance.model.Investment;
import com.javazombiez.finance.service.InvestmentServiceImpl;

public class testInvestmentService {

  @Test
  public void testInvestmentCalculator() {
    Investment investment = new Investment( 100, 12, 10 );
    InvestmentServiceImpl investmentForecast = new InvestmentServiceImpl();
    Forecast forcast = investmentForecast.calculateFutureValueAtFixedRate( investment );
  }

  @Test
  public void testInvestmentCalculatorDifferentInterestRate() {
    InvestmentServiceImpl investmentForecast = new InvestmentServiceImpl();
    Investment investment = new Investment( 100, 12, 8 );
    Forecast forcast = investmentForecast.calculateFutureValueAtFixedRate( investment );
  }
  @Test
  public void testInvestmentCalculatorDifferentTerm() {
    InvestmentServiceImpl investmentForecast = new InvestmentServiceImpl();
    Investment investment = new Investment( 100, 21, 10 );
    Forecast forcast = investmentForecast.calculateFutureValueAtFixedRate( investment );
  }
}
