package com.javazombiez.finance.utility;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test the money formatter class
 * @author Teddy
 *
 */
public class TestMoneyFormatter {

  @Test
  public void testWithTwoDecimals() {
    String money1 = "23.56";
    String money2 = "123.16";
    assertEquals( "23.56", MoneyFormatter.format( money1 ) );
    assertEquals( "123.16", MoneyFormatter.format( money2 ) );
  }

  @Test
  public void testWithOnDecimal() {
    String money1 = "23.5";
    assertEquals( "23.50", MoneyFormatter.format( money1 ) );
    String money2 = "123.0";
    assertEquals( "123.00", MoneyFormatter.format( money2 ) );
  }
  @Test
  public void testNoDecimal(){
    String money1 = "23";
    assertEquals( "23.00", MoneyFormatter.format( money1 ) );
    String money2 = "1";
    assertEquals( "1.00", MoneyFormatter.format( money2 ) );
  }
  
  @Test
  public void testThousandSpaceSeparator() {
    String money1 = "1023.50";
    assertEquals("1,023.50", MoneyFormatter.format(money1));
    String money2 = "1845023.50";
    assertEquals("1,845,023.50", MoneyFormatter.format(money2));
    String money3 = "1235823.50";
    assertEquals("1,235,823.50", MoneyFormatter.format(money3));
    String money4 = "159023.00";
    assertEquals("159,023.00", MoneyFormatter.format(money4));
  }
}
