package com.javazombiez.finance.utility;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test the money class
 * 
 * @author Teddy
 *
 */
public class TestMoney {

  @Test
  public void testAdd() {
    System.out.println( new Money(253255446.02) );
    Money sum1 = new Money( 1 ).add( new Money( 1 ) );
    if( ! sum1.equals( new Money( 2 ) ) )
      fail( "failed to add 1 and 1" );
    Money sum2 = new Money( 1.05 ).add( new Money( 1.04 ) );
    if( ! sum2.equals( new Money( 2.09 ) ) )
      fail( "failed to add 1.05 and 1.04" );
    Money sum3 = new Money( 1.056 ).add( new Money( 1.17 ) );
    if( ! sum3.equals( new Money( 2.23 ) ) )
      fail( "failed to add 1.056 and 1.17" );
  }
}
