#JavaZombiez@Psybergate.finance#

A mini personal-finance system giving a forecast of a typical investment growth compounding over time at fixed & variable interest rates,
in addition, also lays out a forecast of loan/bond repayments and clears the picture of how things actually work with debt, interest rates,
compound interest, tax etc.

The purpose of this simple project was largely focused on aquiring the basics and financial literacy in general, how to manage money,
knowlege that every person MUST have in their lives. At the same time the web-app was written as part of reinforcing and solidifying 
the basics of what we had learned as part of the 2017 Psybergate mentoring programme. 

Technologies used : 

JavaSE 8, Hibernate, JEE 7, EJB 3.1 , Maven, PostgreSQL, JPA, Eclipse IDE, XML, WildFly 10, JavaServer Pages, Git with BitBucket,
JUnit 4, JavaScript, CSS & HTML5.